<?php
   session_start();
   if( isset($_SESSION['admitted_xsisp']) ){      
      $objSys->registroLog($_SESSION['xlogin_id_sisp'], '--', '--', 'Salir');      
   }
   // Se eliminan las variables de sesi�n
   session_unset();
   session_destroy();
   unset($_SESSION['admitted_xsisp']);
   unset($_SESSION['xlogin_id_sisp']);
   // Se redireccion al login
   header('Location: ' . $objSys->GetServer() . 'login.php');
?>
