<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de parámetros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una búsqueda.
 * @param int colSort, contiene el índice de la columna por la cual se ordenarán los datos.
 * @param string typeSort, define el tipo de ordenación de los datos: ASC o DESC.
 * @param int rowStart, especifica el número de fila por la cual inicia la paginación del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrarán en cada página del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mcs_conflictos.class.php';
    $objSys = new System();
    $objDataGridConflictos = new OpetblMcsConflictos();

    //--------------------- Recepción de parámetros --------------------------//
    // Búsqueda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {

        $sql_where = 'c.id_conflicto LIKE ? '
					. 'OR r.region LIKE ? '
					. 'OR m.Municipio LIKE ? '
					. 'OR c.localidad LIKE ? '
					. 'OR c.fecha_conflicto LIKE ? '
					. 'OR c.hora_inicio LIKE ? '
                    . 'OR c.hora_fin LIKE ? '
					. 'OR tm.tipo_movimiento LIKE ? '
					. 'OR og.organizacion_grupo LIKE ? '
					. 'OR c.hechos_text LIKE ? ';

        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }
    // Ordenación...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'c.id_conflicto ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
         $sql_order = 'r.region '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'm.Municipio ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'c.localidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'c.fecha_conflicto ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'c.hora_inicio  ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 7) {
        $sql_order = 'c.hora_fin  ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 8) {
        $sql_order = 'tm.tipo_movimiento ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 9) {
        $sql_order = 'og.organizacion_grupo ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 10) {
        $sql_order = 'c.hechos_text ' . $_GET['typeSort'];
    }

    // Paginación...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridConflictos->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {

            $id_crypt = $objSys->encrypt($dato["id_conflicto"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_conflicto"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                $html .= '  <input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';

                $html .= '<td style="text-align: center; width: 6%; font-size:13px; color:#FF0000;"><strong>'.$dato["id_conflicto"].'</strong></td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["region"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["Municipio"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["localidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 6%;">' . $dato["fecha_conflicto"] . '</td>';
                $html .= '<td style="text-align: center; width: 5%;">' . $dato["hora_inicio"] . '</td>';
                $html .= '<td style="text-align: center; width: 5%;">' . $dato["hora_fin"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["tipo_movimiento"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["organizacion_grupo"] . '</td>';
                $html .= '<td style="text-align: left; width: 15%;" title="'. str_replace('"','*',$dato["hechos_text"]).'">' . substr($dato["hechos_text"],0,150) .' ... </td>';
                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("conflictos_sociales_edit") . "&id=" . $id_crypt;
                //$url_del = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("incidentes_panel") . "&id=" . $id_crypt;
                $html .= '<td style="width: 10%; text-align: center;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Editar Incidente ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" title="Eliminar Incidente ..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="Eliminar" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//... '.$dato["hechos_text"] .'
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de incidentes ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesión...";
}
?>