<?php
/**
 * Complemento ajax para guardar los datos del los vehiculos involucrados en incidentes
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/mysql.class.php';  
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';   
    include $path . 'includes/class/opetbl_mid_incidentes_generalidades.class.php';    
    
    $conexBD = new MySQLPDO();
    $objUsr  = new Usuario(); 
    $objSys  = new System(); 
    $objGen  = new OpetblMidIncidentesGeneralidades();           
    
    // Datos
    $objGen->id_igeneralidad            = $_POST["txtIdIgeneralidad"];                    
    $objGen->id_folio_incidente         = $_POST["txtFolioIncidente"];
    $objGen->id_rol_generalidad         = $_POST["cbxIdRolGeneralidad"];
    $objGen->id_generalidad             = $_POST["cbxIdCategoria"];
    $objGen->id_unidad                  = $_POST["cbxIdUnidad"];
    $objGen->Cantidad                   = $_POST["txtCantidad"];        
    $objGen->Observaciones              = strtoupper( $_POST["txtObservaciones"] );    
    
    //se ejecuta la operacion de ingreso o actualizacion
    $conexBD->beginTransaction();   
    if( $_POST["oper"] == 1 ){
        $result = $objGen->update();      
        $oper = "Upd";
        $ajx_datos['error'] = "actualiza " . $_POST["txtIdIgeneralidad"];
    }elseif( $_POST["oper"] == 2 ){
        $result = $objGen->insert();          
        $oper = "Ins";        
        $ajx_datos['error'] = "inserta" . $_POST["txtIdIgeneralidad"];
    } 
    //se verifica si hubo exito en la consulta de insercion o actualizacion del registro
    if( $result ){
        $objSys->registroLog($objUsr->idUsr, 'opetbl_mid_incidentes_generalidades', $objGen->id_igeneralidad, $oper);                   
        $ajx_datos['rslt']  = true;                
        //$ajx_datos['error'] = "bien";        
        $conexBD->commit();
    }else{
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objGen->msjError;
        //$ajx_datos['error'] = "mal";          
        $conexBD->rollBack();             
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;    
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>