$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*** Ajusta el tama�o del contenedor del grid... ***/
    $('#dvGridIncidentesGeneralidades').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridIncidentesGeneralidades').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xAjaxParam : "id_folio_incidente="+$('#id_folio_incidente').val(),
        xTypeDataAjax: 'json',
    });


/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/






/********************************************************************/
/** Convierte a may�sculas el contenido de los textbox y textarea. **/
/********************************************************************/
$('#txtCantidad,#txtObservaciones').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/

/***** Aplicaci�n de las reglas de validaci�n de los campos del formulario *****/
$('#frmIncidentesGeneralidades').validate({
      rules: {
      /***** INICIO de Reglas de validacion para el formulario *****/
        txtIdIgeneralidad:{
            required:true,digits:true,
        },
        cbxIdRolGeneralidad:{
            required:true,min: 1,
        },
        cbxIdGeneralidad:{
            required:true,min: 1,
        },
        cbxIdUnidad:{
            required:true,min: 1,
        },
        txtCantidad:{
            required:true,digits: true,
        },
        txtObservaciones:{
            required:true,
        },
      /***** FIN de Reglas de validacion para el formulario *****/
     },
     messages:{
     /***** INICIO de Mensajes de validacion para el formulario *****/
        txtIdIgeneralidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
        },
        cbxIdRolGeneralidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdGeneralidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxIdUnidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtCantidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
        },
        txtObservaciones:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
     /***** FIN de Mensajes de validacion para el formulario *****/
     },
      errorClass: "help-inline",
      errorElement: "span",
      highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
      },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
      }
/***** Fin de validacion *****/
});

/********************************************************************/
/***** Control del formulario para agregar/modifiar una persona *****/
/********************************************************************/
    $('#dvFormIncidentesGen').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        buttons: {
            'Guardar': function(){
                guardarGeneralidad();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });

    //mostrar el formulario de alta de generalidades
    $("#btnAgregar, .classModificar").live("click", function(e){

        if( $(this).attr('id') != 'btnAgregar' ){
            $("#id_gen").val( $(this).attr('id').substring(4) );
        }else{
            $("#id_gen").val('');
        }
        $.ajax({
            url: xDcrypt( $('#hdnUrlGen').val() ),
            dataType: 'json',
            data: { 'id_gen' : $("#id_gen").val(), 'id_folio_incidente' : $("#id_folio_incidente").val() },
            type: 'POST',
            async: true,
            cache: false,
            success: function ( xdata ) {
                $("#dvIncGenCont").html( xdata.html );
                $('#dvFormIncidentesGen').dialog('option','title',"Datos del registro");
                $('#dvFormIncidentesGen').dialog('open');
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });

    });


    // $('#dvGridGeneralidades a.lnkBtnOpcionGrid').live('click', function(){
    //     var xId = $(this).attr("rel").split('-');
    //     if( xId[0] == 'edt' ){
    //         $('#hdnIdRef').val( xId[1] );
    //         $('#hdnTypeOper').val('2');
    //         $('#dvFormReferencia').dialog('open');
    //     }
    // });

    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGridGeneralidades table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGridGeneralidades table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');

    // Control para la carga din�mica de las categorias
    $('#cbxIdTipo').live("change", function(e){
        obtenerCategorias( $("#cbxIdTipo").val() );
    });

    // Control para la carga din�mica de las unidades de las generalidades
    $('#cbxIdCategoria').live("change", function(e){
        obtenerUnidades( $("#cbxIdCategoria").val() );
    });

/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/

/***** Fin del document.ready *****/
});

//funcion para guardar los registros de las generalidades
function guardarGeneralidad(){
    var valida = $('#frmIncidentesGen').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Desea guardar los datos actuales?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    $.ajax({
                        url: xDcrypt( $('#hdnUrlSave').val() ),
                        data: $('#frmIncidentesGen').serialize(),
                        type: 'POST',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        success: function (xdata) {
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                frmPreg.dialog('close');
                                xGrid.refreshGrid();
                                $('#dvFormIncidentesGen').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                                frmPreg.dialog('close');
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

//funcion que obtiene los diferentes calibres
function obtenerCategorias( id_tipo ){
    $.ajax({
        url: xDcrypt( $('#hdnUrlCategorias').val() ),
        data: {'id_tipo': id_tipo},
        dataType: 'json',
        type: 'post',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxIdCategoria').html('<option>Cargando categorias...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxIdCategoria').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

//funcion que obtiene los diferentes unidades
    function obtenerUnidades( id_generalidad ){
        $.ajax({
            url: xDcrypt( $('#hdnUrlUnidades').val() ),
            data: {'id_generalidad': id_generalidad},
            dataType: 'json',
            type: 'post',
            async: true,
            cache: false,
            beforeSend: function () {
                $('#cbxIdUnidad').html('<option>Cargando unidades...</option>');
            },
            success: function (xdata) {
                if (xdata.rslt)
                    $('#cbxIdUnidad').html(xdata.html);
                else
                    shwError(xdata.error);
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }