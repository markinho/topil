$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });

/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
//$('#incidentes_panel').disabled();

/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/


/********************************************************************/
/** Convierte a may�sculas el contenido de los textbox y textarea. **/
/********************************************************************/


/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/

/***** Aplicaci�n de las reglas de validaci�n de los campos del formulario *****/



/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/


/***** Fin del document.ready *****/
});

