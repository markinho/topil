$(document).ready(function(){
// alert("Hola desde:");
// alert(get_location());

    //latitud 17.551535
    //longitud -99.500632

    // if (navigator.geolocation) {
    //    navigator.geolocation.getCurrentPosition(mostrarUbicacion);
    // } else {
    //     alert("Error! El navegador no soporta Geolocalizacion.");//3
    // }

    //  function mostrarUbicacion(posicion){
    //     var latitud = posicion.coords.latitude;
    //     var longitud = posicion.coords.longitude;
    //     //alert('Lat: '+latitud+' Lon: '+longitud);
    // }

        //location: { latitude: $('#txtLatitud').val() , longitude: $('#txtLongitud').val() },
    $('#mapaIncidente').locationpicker({
        location: { latitude: $('#txtLatitud').val() , longitude: $('#txtLongitud').val() },
        locationName: $('#txtDireccion').val() ,
        radius: 200,
        zoom: 15,
        scrollwheel: true,
        inputBinding: {
            latitudeInput: $('#txtLatitud'),
            longitudeInput: $('#txtLongitud'),
            radiusInput: $('#txtRadio'),
            locationNameInput: $('#txtDireccion'),
            icono:"police_maker.png"
        },
        enableAutocomplete: true,
        onchanged: function(currentLocation, radius, isMarkerDropped) {
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    });

});//fin de document.ready