<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
 header('content-type: text/html; charset=UTF-8');
//
$path = '';
require_once $path . 'includes/class/config/mysql.class.php';
include 'includes/class/opetbl_mid_incidentes.class.php';


$conexBD        = new MySQLPDO();
$objInc         = new OpetblMidIncidentes();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Incidentes',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="ope/mid/_js/incidentes_rg.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    // datos del incidente

    $objInc->id_folio_incidente         = $_POST["txtIdFolioIncidente"];
    $objInc->prioridad                  = $_POST["RadioGroupPrioridad"];
    $objInc->fecha_incidente            = $objSys->convertirFecha($_POST["txtFechaIncidente"], 'yyyy-mm-dd');
    $fecha                              = explode("/", $_POST["txtFechaIncidente"]);
    $objInc->dia                        = $fecha[2];
    $objInc->mes                        = $fecha[1];
    $objInc->annio                      = $fecha[0];
    $objInc->hora_incidente             = $_POST["selHoras"] . ":" . $_POST["selMinutos"];

    $objInc->id_estado                  = $_POST["cbxIdEstado"];
    //echo "->" . $_POST["cbxIdEstado"];
    $objInc->id_region                  = $_POST["cbxIdRegion"];
    $objInc->id_cuartel                 = $_POST["cbxIdCuartel"];
    $objInc->id_municipio               = $_POST["cbxIdMunicipio"];
    $objInc->localidad                  = $_POST["txtLocalidad"];
    $objInc->colonia                    = $_POST["txtColonia"];
    $objInc->calle                      = $_POST["txtCalle"];
    $objInc->id_asunto                  = $_POST["cbxIdAsunto"];
    $objInc->id_fuente                  = $_POST["cbxIdFuente"];
    $objInc->latitud                    = $_POST["txtLatitud"];
    $objInc->longitud                   = $_POST["txtLongitud"];
    $objInc->direccion                  = $_POST["txtDireccion"];
    $objInc->hechos_html                = $_POST["txtHechosHtml"];
    $objInc->hechos_text                = preg_replace('/\s\s+/', ' ',(strip_tags( $_POST["txtHechosHtml"] )));
    $objInc->ip_usuario                 = $_SERVER['REMOTE_ADDR'];
    $objInc->id_usuario                 = $objUsr->idUsr;

    $conexBD->beginTransaction();

    // Inicia la transacci�n
    if($_POST["oper"] == 1){
        $result = $objInc->update();
        $des_oper = "Upd";
        $url_atras = $objSys->encrypt("incidentes_panel");
    }else{
        $result = $objInc->insert();
        $des_oper = "Ins";
        $url_atras = $objSys->encrypt("index");
    }

    //-------------------------------------------------------------------//
    if ( $result ){
        $objSys->registroLog($objUsr->idUsr, 'opetbl_mid_incidentes', $objInc->id_folio_incidente , $des_oper);
        $conexBD->commit();
        $error = '';
    }else {
        $conexBD->rollBack();
        $error = (!empty($objInc->msjError)) ? $objInc->msjError : 'Error al guardar el incidente.';
    }

    $mod = (empty($error)) ? $url_atras : $objSys->encrypt("incidentes");
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod . "&id_folio_incidente=" . $objSys->encrypt($objInc->id_folio_incidente) ;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>