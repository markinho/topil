<?php
/**
 *
 */
class AdmtblVacaciones
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $anio; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $periodo; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_ini; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $num_dias; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_reg; /** @Tipo: datetime, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $folio; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_soporte; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_soporte; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firmante_soporte; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cargo_firmante; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp, $anio, $periodo)
    {
        $sql = "SELECT curp, anio, periodo, fecha_ini, fecha_fin, num_dias, observaciones, fecha_reg, folio, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante
                FROM admtbl_vacaciones
                WHERE curp=:curp AND anio=:anio AND periodo=:periodo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp, ':anio' => $anio, ':periodo' => $periodo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->anio = $data['anio'];
            $this->periodo = $data['periodo'];
            $this->fecha_ini = $data['fecha_ini'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->num_dias = $data['num_dias'];
            $this->observaciones = $data['observaciones'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->folio = $data['folio'];
            $this->no_oficio = $data['no_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->no_soporte = $data['no_soporte'];
            $this->fecha_soporte = $data['fecha_soporte'];
            $this->firmante_soporte = $data['firmante_soporte'];
            $this->cargo_firmante = $data['cargo_firmante'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
     /**
     * Funci�n para seleccionar los datos  de la tabla admtbl_cambios_temporales para el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @return array datos para el grid de tramites de vacaciones
     */
    public function selectListTramite($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select curp, anio, periodo, fecha_ini, fecha_fin, num_dias, no_oficio,fecha_oficio 
                from admtbl_vacaciones 
                ";
        if (!empty($sqlWhere))
            $sql .= " WHERE  $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'anio' => $data['anio'],
                               'periodo' => $data['periodo'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'num_dias' => $data['num_dias'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.anio, a.periodo, a.fecha_ini, a.fecha_fin, a.num_dias, a.observaciones, a.fecha_reg, a.folio, a.no_oficio, a.fecha_oficio, a.no_soporte, a.fecha_soporte, a.firmante_soporte, a.cargo_firmante,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status
                FROM admtbl_vacaciones a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'anio' => $data['anio'],
                               'periodo' => $data['periodo'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'num_dias' => $data['num_dias'],
                               'observaciones' => $data['observaciones'],
                               'fecha_reg' => $data['fecha_reg'],
                               'folio' => $data['folio'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'no_soporte' => $data['no_soporte'],
                               'fecha_soporte' => $data['fecha_soporte'],
                               'firmante_soporte' => $data['firmante_soporte'],
                               'cargo_firmante' => $data['cargo_firmante'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_vacaciones(curp, anio, periodo, fecha_ini, fecha_fin, num_dias, observaciones, fecha_reg, folio, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante)
                VALUES(:curp, :anio, :periodo, :fecha_ini, :fecha_fin, :num_dias, :observaciones, :fecha_reg, :folio, :no_oficio, :fecha_oficio, :no_soporte, :fecha_soporte, :firmante_soporte, :cargo_firmante);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":anio" => $this->anio, ":periodo" => $this->periodo, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":num_dias" => $this->num_dias, ":observaciones" => $this->observaciones, ":fecha_reg" => date("Y-m-d"), ":folio" => $this->folio, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante));
            if ($qry)
                return true;
                
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_vacaciones
                   SET fecha_ini=:fecha_ini, fecha_fin=:fecha_fin, num_dias=:num_dias, observaciones=:observaciones,  folio=:folio, no_oficio=:no_oficio, fecha_oficio=:fecha_oficio, no_soporte=:no_soporte, fecha_soporte=:fecha_soporte, firmante_soporte=:firmante_soporte, cargo_firmante=:cargo_firmante
                WHERE curp=:curp AND anio=:anio AND periodo=:periodo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":anio" => $this->anio, ":periodo" => $this->periodo, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":num_dias" => $this->num_dias, ":observaciones" => $this->observaciones, ":folio" => $this->folio, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

   public function delete()
    {
      $sql = "DELETE FROM admtbl_vacaciones
                WHERE curp=:curp 
                AND anio=:anio AND periodo=:periodo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":anio" => $this->anio, ":periodo" => $this->periodo));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }

    }
}


?>