<?php
/**
 *
 */
class Xcatperfiles
{
    public $id_perfil; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $perfil; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $tipo; /** @Tipo: enum('General','Personalizado'), @Acepta Nulos: NO, @Llave: --, @Default: General */
    public $stat; /** @Tipo: TINYINT(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    
    public $msjError; // almacena el mensaje de error si éste ocurre
    private $_conexBD; // objeto de conexión a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Función para mostrar la lista de perfiles de usuarios dentro de un combobox.
     * @param int $id, id del perfil seleccionado por deafult 
     * @return array html(options)
     */
    public function shwPerfiles($id=0, $sqlWhere=''){
        $aryDatos = $this->selectAll($sqlWhere, 'perfil Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            $aryPermisos = $this->getPermisos($datos["id_perfil"]);
            $data_perm= '';
            foreach ($aryPermisos as $key => $permiso) {
                $data_perm .= '['.$permiso['permiso'].'] '.$permiso['descripcion'];
                if( count($aryPermisos) > ($key + 1) )
                    $data_perm .= '|';
            }

            if( $id == $datos["id_perfil"] )
                $html .= '<option value="'.$datos["id_perfil"].'" data-permisos="'.$data_perm.'" selected>'.$datos["perfil"].'</option>';
            else
                $html .= '<option value="'.$datos["id_perfil"].'" data-permisos="'.$data_perm.'">'.$datos["perfil"].'</option>';
        }
        return $html;
    }

    /**
     * Función para obtener un registro específico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realizó con éxito
     */
    public function select($id_perfil)
    {
        $sql = "SELECT id_perfil, perfil, tipo, stat 
                FROM xcatperfiles
                WHERE id_perfil=:id_perfil;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_perfil' => $id_perfil));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_perfil = $data['id_perfil'];
            $this->perfil = $data['perfil'];
            $this->tipo = $data['tipo'];
            $this->stat = $data['stat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selección de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_perfil, a.perfil, a.tipo, a.stat 
                FROM xcatperfiles a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_perfil' => $data['id_perfil'],
                               'perfil' => $data['perfil'],
                               'tipo' => $data['tipo'],
                               'stat' => $data['stat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el último id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xcatperfiles(id_perfil, perfil, tipo, stat)
                VALUES(:id_perfil, :perfil, :tipo, :stat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_perfil" => $this->id_perfil, ":perfil" => $this->perfil, ":tipo" => $this->tipo, ":stat" => $this->stat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xcatperfiles
                   SET perfil=:perfil, tipo=:tipo, stat=:stat
                WHERE id_perfil=:id_perfil;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_perfil" => $this->id_perfil, ":perfil" => $this->perfil, ":tipo" => $this->tipo, ":stat" => $this->stat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }

    private function getPermisos($id_perfil, $sqlWhere='', $sqlOrder='')
    {
        $sql = "SELECT a.id_perfil, a.id_permiso, b.permiso, b.descripcion, b.tipo, b.stat
                FROM xtblperfil_permisos a
                    INNER JOIN xcatpermisos b ON a.id_permiso = b.id_permiso
                WHERE a.id_perfil=:id_perfil AND b.stat = 1 ";
        if (!empty($sqlWhere))
            $sql .= "AND $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";        
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute( array(":id_perfil" => $id_perfil) );
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_perfil' => $data['id_perfil'],
                               'id_permiso' => $data['id_permiso'],
                               'permiso' => $data['permiso'],
                               'descripcion' => $data['descripcion'],
                               'tipo' => $data['tipo'],
                               'stat' => $data['stat'],                               
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>