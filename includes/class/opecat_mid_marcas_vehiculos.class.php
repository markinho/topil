<?php
/**
 *
 */
class OpecatMidMarcasVehiculos
{
    public $id_marca; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $marca; /** @Tipo: varchar(80), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_marca)
    {
        $sql = "SELECT id_marca, marca
                FROM opecat_mid_marcas_vehiculos
                WHERE id_marca=:id_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_marca = $data['id_marca'];
            $this->marca = $data['marca'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_marca, a.marca
                FROM opecat_mid_marcas_vehiculos a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_marca' => $data['id_marca'],
                               'marca' => $data['marca'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_marcas_vehiculos(id_marca, marca)
                VALUES(:id_marca, :marca);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":marca" => $this->marca));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_marcas_vehiculos
                   SET marca=:marca
                WHERE id_marca=:id_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":marca" => $this->marca));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>