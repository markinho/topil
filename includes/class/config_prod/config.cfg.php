<?php

/**
 * @author CSITI
 * @copyright 2014
 * 
 */
// Software
define('DEBUG', true);
define('APP_NAME', 'TopilNet');
define('APP_DESCRIPTION', 'Sistema Integral de Seguridad Pública');
define('APP_VERSION', '1.5');
define('APP_POWEREDBY', 'SBD');
define('APP_AUTHORSITE', 'http://sbd.com.mx');

// Configuración del nombre de dominio
$host = $_SERVER['HTTP_HOST'].'/';
if (preg_match("/www/i", $host))    $host = str_replace('www.', '', $host);
if (preg_match("/topil./i", $host))  $host = str_replace('topil.', '', $host);
if (!preg_match("/http:/i", $host)) $host = 'http://'.$host;
define('SERVER_NAME', $host);
define('SUBDOMAIN', 'topil/');

// Rutas
define('PATH_IMAGES', 'includes/css/imgs/');

// Personalización del sistema
define('INSTITUTION_NAME', 'Secretaría de Seguridad Pública Estatal');

// Seguridad
define('ENCRYPT', false);
define('ENCRYPT_PSWD', '');

?>