<?php
/**
 *
 */
class LogtblVehDetalles
{
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $kilometraje; /** @Tipo: mediumint(6), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $espejo_der; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $espejo_izq; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $espejo_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cuarto_der; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cuarto_izq; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cuarto_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fanal_der; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fanal_izq; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fanal_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $calavera_der; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $calavera_izq; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $calavera_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $moldura_der; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $moldura_izq; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $moldura_del; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $moldura_tras; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $emblema_der; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $emblema_izq; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $emblema_del; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $emblema_tras; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $pintura; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $faros_antiniebla; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $limpiadores; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $limpiadores_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $espejo_interior; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $espejo_interior_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $encendedor; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cinturon; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tapetes; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $disco_compacto; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $antena; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $manijas; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cenicero; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $claxon; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $radio_amfm; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $vestiduras; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $otros; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $gato; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $maneral; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $reflejantes; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cable_pc; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $llave_espanola; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $desarmador_plano; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $extintor; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $llave_cruz; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $llave_l; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $pinzas; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $llave_allen; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $desarmador_cruz; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $llave_bujia; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tapones; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $rines_acero; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $torreta; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $torreta_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $radio_movil; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $radio_movil_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $alto_parlante; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $alto_parlante_estado; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $protector_frontal; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $lona; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $rin_aluminio; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $aa; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $roll_bar; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $redilas; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $llantas_cantidad; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_medida_llantas; /** @Tipo: tinyint(3), @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_marca_llantas; /** @Tipo: tinyint(3), @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $llantas_vida_util; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatVehMedidaLlantas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogtblVehiculos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehMarcaLlantas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_veh_medida_llantas.class.php';
        require_once 'logtbl_vehiculos.class.php';
        require_once 'logcat_veh_marca_llantas.class.php';
        $this->LogcatVehMedidaLlantas = new LogcatVehMedidaLlantas();
        $this->LogtblVehiculos = new LogtblVehiculos();
        $this->LogcatVehMarcaLlantas = new LogcatVehMarcaLlantas();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_vehiculo)
    {
        $sql = "SELECT id_vehiculo, kilometraje, espejo_der, espejo_izq, espejo_estado, cuarto_der, cuarto_izq, cuarto_estado, fanal_der, fanal_izq, fanal_estado, calavera_der, calavera_izq, calavera_estado, moldura_der, moldura_izq, moldura_del, moldura_tras, emblema_der, emblema_izq, emblema_del, emblema_tras, pintura, faros_antiniebla, limpiadores, limpiadores_estado, espejo_interior, espejo_interior_estado, encendedor, cinturon, tapetes, disco_compacto, antena, manijas, cenicero, claxon, radio_amfm, vestiduras, otros, gato, maneral, reflejantes, cable_pc, llave_espanola, desarmador_plano, extintor, llave_cruz, llave_l, pinzas, llave_allen, desarmador_cruz, llave_bujia, tapones, rines_acero, torreta, torreta_estado, radio_movil, radio_movil_estado, alto_parlante, alto_parlante_estado, protector_frontal, lona, rin_aluminio, aa, roll_bar, redilas, llantas_cantidad, id_medida_llantas, id_marca_llantas, llantas_vida_util
                FROM logtbl_veh_detalles
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo' => $id_vehiculo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->kilometraje = $data['kilometraje'];
            $this->espejo_der = $data['espejo_der'];
            $this->espejo_izq = $data['espejo_izq'];
            $this->espejo_estado = $data['espejo_estado'];
            $this->cuarto_der = $data['cuarto_der'];
            $this->cuarto_izq = $data['cuarto_izq'];
            $this->cuarto_estado = $data['cuarto_estado'];
            $this->fanal_der = $data['fanal_der'];
            $this->fanal_izq = $data['fanal_izq'];
            $this->fanal_estado = $data['fanal_estado'];
            $this->calavera_der = $data['calavera_der'];
            $this->calavera_izq = $data['calavera_izq'];
            $this->calavera_estado = $data['calavera_estado'];
            $this->moldura_der = $data['moldura_der'];
            $this->moldura_izq = $data['moldura_izq'];
            $this->moldura_del = $data['moldura_del'];
            $this->moldura_tras = $data['moldura_tras'];
            $this->emblema_der = $data['emblema_der'];
            $this->emblema_izq = $data['emblema_izq'];
            $this->emblema_del = $data['emblema_del'];
            $this->emblema_tras = $data['emblema_tras'];
            $this->pintura = $data['pintura'];
            $this->faros_antiniebla = $data['faros_antiniebla'];
            $this->limpiadores = $data['limpiadores'];
            $this->limpiadores_estado = $data['limpiadores_estado'];
            $this->espejo_interior = $data['espejo_interior'];
            $this->espejo_interior_estado = $data['espejo_interior_estado'];
            $this->encendedor = $data['encendedor'];
            $this->cinturon = $data['cinturon'];
            $this->tapetes = $data['tapetes'];
            $this->disco_compacto = $data['disco_compacto'];
            $this->antena = $data['antena'];
            $this->manijas = $data['manijas'];
            $this->cenicero = $data['cenicero'];
            $this->claxon = $data['claxon'];
            $this->radio_amfm = $data['radio_amfm'];
            $this->vestiduras = $data['vestiduras'];
            $this->otros = $data['otros'];
            $this->gato = $data['gato'];
            $this->maneral = $data['maneral'];
            $this->reflejantes = $data['reflejantes'];
            $this->cable_pc = $data['cable_pc'];
            $this->llave_espanola = $data['llave_espanola'];
            $this->desarmador_plano = $data['desarmador_plano'];
            $this->extintor = $data['extintor'];
            $this->llave_cruz = $data['llave_cruz'];
            $this->llave_l = $data['llave_l'];
            $this->pinzas = $data['pinzas'];
            $this->llave_allen = $data['llave_allen'];
            $this->desarmador_cruz = $data['desarmador_cruz'];
            $this->llave_bujia = $data['llave_bujia'];
            $this->tapones = $data['tapones'];
            $this->rines_acero = $data['rines_acero'];
            $this->torreta = $data['torreta'];
            $this->torreta_estado = $data['torreta_estado'];
            $this->radio_movil = $data['radio_movil'];
            $this->radio_movil_estado = $data['radio_movil_estado'];
            $this->alto_parlante = $data['alto_parlante'];
            $this->alto_parlante_estado = $data['alto_parlante_estado'];
            $this->protector_frontal = $data['protector_frontal'];
            $this->lona = $data['lona'];
            $this->rin_aluminio = $data['rin_aluminio'];
            $this->aa = $data['aa'];
            $this->roll_bar = $data['roll_bar'];
            $this->redilas = $data['redilas'];
            $this->llantas_cantidad = $data['llantas_cantidad'];
            $this->id_medida_llantas = $data['id_medida_llantas'];
            $this->id_marca_llantas = $data['id_marca_llantas'];
            $this->llantas_vida_util = $data['llantas_vida_util'];

            $this->LogcatVehMedidaLlantas->select($this->id_medida_llantas);
            $this->LogtblVehiculos->select($this->id_vehiculo);
            $this->LogcatVehMarcaLlantas->select($this->id_marca_llantas);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_vehiculo, a.kilometraje, a.espejo_der, a.espejo_izq, a.espejo_estado, a.cuarto_der, a.cuarto_izq, a.cuarto_estado, a.fanal_der, a.fanal_izq, a.fanal_estado, a.calavera_der, a.calavera_izq, a.calavera_estado, a.moldura_der, a.moldura_izq, a.moldura_del, a.moldura_tras, a.emblema_der, a.emblema_izq, a.emblema_del, a.emblema_tras, a.pintura, a.faros_antiniebla, a.limpiadores, a.limpiadores_estado, a.espejo_interior, a.espejo_interior_estado, a.encendedor, a.cinturon, a.tapetes, a.disco_compacto, a.antena, a.manijas, a.cenicero, a.claxon, a.radio_amfm, a.vestiduras, a.otros, a.gato, a.maneral, a.reflejantes, a.cable_pc, a.llave_espanola, a.desarmador_plano, a.extintor, a.llave_cruz, a.llave_l, a.pinzas, a.llave_allen, a.desarmador_cruz, a.llave_bujia, a.tapones, a.rines_acero, a.torreta, a.torreta_estado, a.radio_movil, a.radio_movil_estado, a.alto_parlante, a.alto_parlante_estado, a.protector_frontal, a.lona, a.rin_aluminio, a.aa, a.roll_bar, a.redilas, a.llantas_cantidad, a.id_medida_llantas, a.id_marca_llantas, a.llantas_vida_util,
                  b.id_medida_llantas, b.medida_llantas, b.xstat,
                  c.id_vehiculo, c.num_serie, c.num_economico, c.num_motor, c.poliza, c.inciso, c.placas, c.num_placas, c.tarjeta_circulacion, c.reg_fed_veh, c.num_puertas, c.num_cilindros, c.foto_frente, c.foto_lat_der, c.foto_lat_izq, c.foto_posterior, c.foto_interior, c.id_estado_fisico, c.id_situacion, c.id_modelo, c.id_transmision, c.id_color, c.id_tipo,
                  d.id_marca_llantas, d.marca_llantas, d.xstat
                FROM logtbl_veh_detalles a 
                 LEFT JOIN logcat_veh_medida_llantas b ON a.id_medida_llantas=b.id_medida_llantas
                 LEFT JOIN logtbl_vehiculos c ON a.id_vehiculo=c.id_vehiculo
                 LEFT JOIN logcat_veh_marca_llantas d ON a.id_marca_llantas=d.id_marca_llantas";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'kilometraje' => $data['kilometraje'],
                               'espejo_der' => $data['espejo_der'],
                               'espejo_izq' => $data['espejo_izq'],
                               'espejo_estado' => $data['espejo_estado'],
                               'cuarto_der' => $data['cuarto_der'],
                               'cuarto_izq' => $data['cuarto_izq'],
                               'cuarto_estado' => $data['cuarto_estado'],
                               'fanal_der' => $data['fanal_der'],
                               'fanal_izq' => $data['fanal_izq'],
                               'fanal_estado' => $data['fanal_estado'],
                               'calavera_der' => $data['calavera_der'],
                               'calavera_izq' => $data['calavera_izq'],
                               'calavera_estado' => $data['calavera_estado'],
                               'moldura_der' => $data['moldura_der'],
                               'moldura_izq' => $data['moldura_izq'],
                               'moldura_del' => $data['moldura_del'],
                               'moldura_tras' => $data['moldura_tras'],
                               'emblema_der' => $data['emblema_der'],
                               'emblema_izq' => $data['emblema_izq'],
                               'emblema_del' => $data['emblema_del'],
                               'emblema_tras' => $data['emblema_tras'],
                               'pintura' => $data['pintura'],
                               'faros_antiniebla' => $data['faros_antiniebla'],
                               'limpiadores' => $data['limpiadores'],
                               'limpiadores_estado' => $data['limpiadores_estado'],
                               'espejo_interior' => $data['espejo_interior'],
                               'espejo_interior_estado' => $data['espejo_interior_estado'],
                               'encendedor' => $data['encendedor'],
                               'cinturon' => $data['cinturon'],
                               'tapetes' => $data['tapetes'],
                               'disco_compacto' => $data['disco_compacto'],
                               'antena' => $data['antena'],
                               'manijas' => $data['manijas'],
                               'cenicero' => $data['cenicero'],
                               'claxon' => $data['claxon'],
                               'radio_amfm' => $data['radio_amfm'],
                               'vestiduras' => $data['vestiduras'],
                               'otros' => $data['otros'],
                               'gato' => $data['gato'],
                               'maneral' => $data['maneral'],
                               'reflejantes' => $data['reflejantes'],
                               'cable_pc' => $data['cable_pc'],
                               'llave_espanola' => $data['llave_espanola'],
                               'desarmador_plano' => $data['desarmador_plano'],
                               'extintor' => $data['extintor'],
                               'llave_cruz' => $data['llave_cruz'],
                               'llave_l' => $data['llave_l'],
                               'pinzas' => $data['pinzas'],
                               'llave_allen' => $data['llave_allen'],
                               'desarmador_cruz' => $data['desarmador_cruz'],
                               'llave_bujia' => $data['llave_bujia'],
                               'tapones' => $data['tapones'],
                               'rines_acero' => $data['rines_acero'],
                               'torreta' => $data['torreta'],
                               'torreta_estado' => $data['torreta_estado'],
                               'radio_movil' => $data['radio_movil'],
                               'radio_movil_estado' => $data['radio_movil_estado'],
                               'alto_parlante' => $data['alto_parlante'],
                               'alto_parlante_estado' => $data['alto_parlante_estado'],
                               'protector_frontal' => $data['protector_frontal'],
                               'lona' => $data['lona'],
                               'rin_aluminio' => $data['rin_aluminio'],
                               'aa' => $data['aa'],
                               'roll_bar' => $data['roll_bar'],
                               'redilas' => $data['redilas'],
                               'llantas_cantidad' => $data['llantas_cantidad'],
                               'id_medida_llantas' => $data['id_medida_llantas'],
                               'id_marca_llantas' => $data['id_marca_llantas'],
                               'llantas_vida_util' => $data['llantas_vida_util'],
                               'logcat_veh_medida_llantas_medida_llantas' => $data['medida_llantas'],
                               'logcat_veh_medida_llantas_xstat' => $data['xstat'],
                               'logtbl_vehiculos_num_serie' => $data['num_serie'],
                               'logtbl_vehiculos_num_economico' => $data['num_economico'],
                               'logtbl_vehiculos_num_motor' => $data['num_motor'],
                               'logtbl_vehiculos_poliza' => $data['poliza'],
                               'logtbl_vehiculos_inciso' => $data['inciso'],
                               'logtbl_vehiculos_placas' => $data['placas'],
                               'logtbl_vehiculos_num_placas' => $data['num_placas'],
                               'logtbl_vehiculos_tarjeta_circulacion' => $data['tarjeta_circulacion'],
                               'logtbl_vehiculos_reg_fed_veh' => $data['reg_fed_veh'],
                               'logtbl_vehiculos_num_puertas' => $data['num_puertas'],
                               'logtbl_vehiculos_num_cilindros' => $data['num_cilindros'],
                               'logtbl_vehiculos_foto_frente' => $data['foto_frente'],
                               'logtbl_vehiculos_foto_lat_der' => $data['foto_lat_der'],
                               'logtbl_vehiculos_foto_lat_izq' => $data['foto_lat_izq'],
                               'logtbl_vehiculos_foto_posterior' => $data['foto_posterior'],
                               'logtbl_vehiculos_foto_interior' => $data['foto_interior'],
                               'logtbl_vehiculos_id_estado_fisico' => $data['id_estado_fisico'],
                               'logtbl_vehiculos_id_situacion' => $data['id_situacion'],
                               'logtbl_vehiculos_id_modelo' => $data['id_modelo'],
                               'logtbl_vehiculos_id_transmision' => $data['id_transmision'],
                               'logtbl_vehiculos_id_color' => $data['id_color'],
                               'logtbl_vehiculos_id_tipo' => $data['id_tipo'],
                               'logcat_veh_marca_llantas_marca_llantas' => $data['marca_llantas'],
                               'logcat_veh_marca_llantas_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_veh_detalles(id_vehiculo, kilometraje, espejo_der, espejo_izq, espejo_estado, cuarto_der, cuarto_izq, cuarto_estado, fanal_der, fanal_izq, fanal_estado, calavera_der, calavera_izq, calavera_estado, moldura_der, moldura_izq, moldura_del, moldura_tras, emblema_der, emblema_izq, emblema_del, emblema_tras, pintura, faros_antiniebla, limpiadores, limpiadores_estado, espejo_interior, espejo_interior_estado, encendedor, cinturon, tapetes, disco_compacto, antena, manijas, cenicero, claxon, radio_amfm, vestiduras, otros, gato, maneral, reflejantes, cable_pc, llave_espanola, desarmador_plano, extintor, llave_cruz, llave_l, pinzas, llave_allen, desarmador_cruz, llave_bujia, tapones, rines_acero, torreta, torreta_estado, radio_movil, radio_movil_estado, alto_parlante, alto_parlante_estado, protector_frontal, lona, rin_aluminio, aa, roll_bar, redilas, llantas_cantidad, id_medida_llantas, id_marca_llantas, llantas_vida_util)
                VALUES(:id_vehiculo, :kilometraje, :espejo_der, :espejo_izq, :espejo_estado, :cuarto_der, :cuarto_izq, :cuarto_estado, :fanal_der, :fanal_izq, :fanal_estado, :calavera_der, :calavera_izq, :calavera_estado, :moldura_der, :moldura_izq, :moldura_del, :moldura_tras, :emblema_der, :emblema_izq, :emblema_del, :emblema_tras, :pintura, :faros_antiniebla, :limpiadores, :limpiadores_estado, :espejo_interior, :espejo_interior_estado, :encendedor, :cinturon, :tapetes, :disco_compacto, :antena, :manijas, :cenicero, :claxon, :radio_amfm, :vestiduras, :otros, :gato, :maneral, :reflejantes, :cable_pc, :llave_espanola, :desarmador_plano, :extintor, :llave_cruz, :llave_l, :pinzas, :llave_allen, :desarmador_cruz, :llave_bujia, :tapones, :rines_acero, :torreta, :torreta_estado, :radio_movil, :radio_movil_estado, :alto_parlante, :alto_parlante_estado, :protector_frontal, :lona, :rin_aluminio, :aa, :roll_bar, :redilas, :llantas_cantidad, :id_medida_llantas, :id_marca_llantas, :llantas_vida_util);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":kilometraje" => $this->kilometraje, ":espejo_der" => $this->espejo_der, ":espejo_izq" => $this->espejo_izq, ":espejo_estado" => $this->espejo_estado, ":cuarto_der" => $this->cuarto_der, ":cuarto_izq" => $this->cuarto_izq, ":cuarto_estado" => $this->cuarto_estado, ":fanal_der" => $this->fanal_der, ":fanal_izq" => $this->fanal_izq, ":fanal_estado" => $this->fanal_estado, ":calavera_der" => $this->calavera_der, ":calavera_izq" => $this->calavera_izq, ":calavera_estado" => $this->calavera_estado, ":moldura_der" => $this->moldura_der, ":moldura_izq" => $this->moldura_izq, ":moldura_del" => $this->moldura_del, ":moldura_tras" => $this->moldura_tras, ":emblema_der" => $this->emblema_der, ":emblema_izq" => $this->emblema_izq, ":emblema_del" => $this->emblema_del, ":emblema_tras" => $this->emblema_tras, ":pintura" => $this->pintura, ":faros_antiniebla" => $this->faros_antiniebla, ":limpiadores" => $this->limpiadores, ":limpiadores_estado" => $this->limpiadores_estado, ":espejo_interior" => $this->espejo_interior, ":espejo_interior_estado" => $this->espejo_interior_estado, ":encendedor" => $this->encendedor, ":cinturon" => $this->cinturon, ":tapetes" => $this->tapetes, ":disco_compacto" => $this->disco_compacto, ":antena" => $this->antena, ":manijas" => $this->manijas, ":cenicero" => $this->cenicero, ":claxon" => $this->claxon, ":radio_amfm" => $this->radio_amfm, ":vestiduras" => $this->vestiduras, ":otros" => $this->otros, ":gato" => $this->gato, ":maneral" => $this->maneral, ":reflejantes" => $this->reflejantes, ":cable_pc" => $this->cable_pc, ":llave_espanola" => $this->llave_espanola, ":desarmador_plano" => $this->desarmador_plano, ":extintor" => $this->extintor, ":llave_cruz" => $this->llave_cruz, ":llave_l" => $this->llave_l, ":pinzas" => $this->pinzas, ":llave_allen" => $this->llave_allen, ":desarmador_cruz" => $this->desarmador_cruz, ":llave_bujia" => $this->llave_bujia, ":tapones" => $this->tapones, ":rines_acero" => $this->rines_acero, ":torreta" => $this->torreta, ":torreta_estado" => $this->torreta_estado, ":radio_movil" => $this->radio_movil, ":radio_movil_estado" => $this->radio_movil_estado, ":alto_parlante" => $this->alto_parlante, ":alto_parlante_estado" => $this->alto_parlante_estado, ":protector_frontal" => $this->protector_frontal, ":lona" => $this->lona, ":rin_aluminio" => $this->rin_aluminio, ":aa" => $this->aa, ":roll_bar" => $this->roll_bar, ":redilas" => $this->redilas, ":llantas_cantidad" => $this->llantas_cantidad, ":id_medida_llantas" => $this->id_medida_llantas, ":id_marca_llantas" => $this->id_marca_llantas, ":llantas_vida_util" => $this->llantas_vida_util));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_veh_detalles
                   SET kilometraje=:kilometraje, espejo_der=:espejo_der, espejo_izq=:espejo_izq, espejo_estado=:espejo_estado, cuarto_der=:cuarto_der, cuarto_izq=:cuarto_izq, cuarto_estado=:cuarto_estado, fanal_der=:fanal_der, fanal_izq=:fanal_izq, fanal_estado=:fanal_estado, calavera_der=:calavera_der, calavera_izq=:calavera_izq, calavera_estado=:calavera_estado, moldura_der=:moldura_der, moldura_izq=:moldura_izq, moldura_del=:moldura_del, moldura_tras=:moldura_tras, emblema_der=:emblema_der, emblema_izq=:emblema_izq, emblema_del=:emblema_del, emblema_tras=:emblema_tras, pintura=:pintura, faros_antiniebla=:faros_antiniebla, limpiadores=:limpiadores, limpiadores_estado=:limpiadores_estado, espejo_interior=:espejo_interior, espejo_interior_estado=:espejo_interior_estado, encendedor=:encendedor, cinturon=:cinturon, tapetes=:tapetes, disco_compacto=:disco_compacto, antena=:antena, manijas=:manijas, cenicero=:cenicero, claxon=:claxon, radio_amfm=:radio_amfm, vestiduras=:vestiduras, otros=:otros, gato=:gato, maneral=:maneral, reflejantes=:reflejantes, cable_pc=:cable_pc, llave_espanola=:llave_espanola, desarmador_plano=:desarmador_plano, extintor=:extintor, llave_cruz=:llave_cruz, llave_l=:llave_l, pinzas=:pinzas, llave_allen=:llave_allen, desarmador_cruz=:desarmador_cruz, llave_bujia=:llave_bujia, tapones=:tapones, rines_acero=:rines_acero, torreta=:torreta, torreta_estado=:torreta_estado, radio_movil=:radio_movil, radio_movil_estado=:radio_movil_estado, alto_parlante=:alto_parlante, alto_parlante_estado=:alto_parlante_estado, protector_frontal=:protector_frontal, lona=:lona, rin_aluminio=:rin_aluminio, aa=:aa, roll_bar=:roll_bar, redilas=:redilas, llantas_cantidad=:llantas_cantidad, id_medida_llantas=:id_medida_llantas, id_marca_llantas=:id_marca_llantas, llantas_vida_util=:llantas_vida_util
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":kilometraje" => $this->kilometraje, ":espejo_der" => $this->espejo_der, ":espejo_izq" => $this->espejo_izq, ":espejo_estado" => $this->espejo_estado, ":cuarto_der" => $this->cuarto_der, ":cuarto_izq" => $this->cuarto_izq, ":cuarto_estado" => $this->cuarto_estado, ":fanal_der" => $this->fanal_der, ":fanal_izq" => $this->fanal_izq, ":fanal_estado" => $this->fanal_estado, ":calavera_der" => $this->calavera_der, ":calavera_izq" => $this->calavera_izq, ":calavera_estado" => $this->calavera_estado, ":moldura_der" => $this->moldura_der, ":moldura_izq" => $this->moldura_izq, ":moldura_del" => $this->moldura_del, ":moldura_tras" => $this->moldura_tras, ":emblema_der" => $this->emblema_der, ":emblema_izq" => $this->emblema_izq, ":emblema_del" => $this->emblema_del, ":emblema_tras" => $this->emblema_tras, ":pintura" => $this->pintura, ":faros_antiniebla" => $this->faros_antiniebla, ":limpiadores" => $this->limpiadores, ":limpiadores_estado" => $this->limpiadores_estado, ":espejo_interior" => $this->espejo_interior, ":espejo_interior_estado" => $this->espejo_interior_estado, ":encendedor" => $this->encendedor, ":cinturon" => $this->cinturon, ":tapetes" => $this->tapetes, ":disco_compacto" => $this->disco_compacto, ":antena" => $this->antena, ":manijas" => $this->manijas, ":cenicero" => $this->cenicero, ":claxon" => $this->claxon, ":radio_amfm" => $this->radio_amfm, ":vestiduras" => $this->vestiduras, ":otros" => $this->otros, ":gato" => $this->gato, ":maneral" => $this->maneral, ":reflejantes" => $this->reflejantes, ":cable_pc" => $this->cable_pc, ":llave_espanola" => $this->llave_espanola, ":desarmador_plano" => $this->desarmador_plano, ":extintor" => $this->extintor, ":llave_cruz" => $this->llave_cruz, ":llave_l" => $this->llave_l, ":pinzas" => $this->pinzas, ":llave_allen" => $this->llave_allen, ":desarmador_cruz" => $this->desarmador_cruz, ":llave_bujia" => $this->llave_bujia, ":tapones" => $this->tapones, ":rines_acero" => $this->rines_acero, ":torreta" => $this->torreta, ":torreta_estado" => $this->torreta_estado, ":radio_movil" => $this->radio_movil, ":radio_movil_estado" => $this->radio_movil_estado, ":alto_parlante" => $this->alto_parlante, ":alto_parlante_estado" => $this->alto_parlante_estado, ":protector_frontal" => $this->protector_frontal, ":lona" => $this->lona, ":rin_aluminio" => $this->rin_aluminio, ":aa" => $this->aa, ":roll_bar" => $this->roll_bar, ":redilas" => $this->redilas, ":llantas_cantidad" => $this->llantas_cantidad, ":id_medida_llantas" => $this->id_medida_llantas, ":id_marca_llantas" => $this->id_marca_llantas, ":llantas_vida_util" => $this->llantas_vida_util));
            if ($qry){ 
                //echo "yes detalles";
				return true;
			}else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>