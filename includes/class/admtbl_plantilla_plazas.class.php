<?php
/**
 *
 */
class AdmtblPlantillaPlazas
{
    public $id_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $folio; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */
    public $id_categoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $fecha_creacion; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatAreas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatCategorias; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_areas.class.php';
        require_once 'admcat_categorias.class.php';
        $this->AdmcatAreas = new AdmcatAreas();
        $this->AdmcatCategorias = new AdmcatCategorias();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_plaza)
    {
        $sql = "SELECT id_plaza, folio, id_categoria, id_area, fecha_creacion, status, observaciones
                FROM admtbl_plantilla_plazas
                WHERE id_plaza=:id_plaza;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_plaza' => $id_plaza));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_plaza = $data['id_plaza'];
            $this->folio = $data['folio'];
            $this->id_categoria = $data['id_categoria'];
            $this->id_area = $data['id_area'];
            $this->fecha_creacion = $data['fecha_creacion'];
            $this->status = $data['status'];
            $this->observaciones = $data['observaciones'];

            $this->AdmcatAreas->select($this->id_area);
            $this->AdmcatCategorias->select($this->id_categoria);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function selectPlaza($folio)
    {
        $sql = "SELECT id_plaza, folio, id_categoria, id_area, fecha_creacion, status, observaciones
                FROM admtbl_plantilla_plazas
                WHERE folio=:folio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':folio' => $folio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_plaza = $data['id_plaza'];
            $this->folio = $data['folio'];
            $this->id_categoria = $data['id_categoria'];
            $this->id_area = $data['id_area'];
            $this->fecha_creacion = $data['fecha_creacion'];
            $this->status = $data['status'];
            $this->observaciones = $data['observaciones'];

            $this->AdmcatAreas->select($this->id_area);
            $this->AdmcatCategorias->select($this->id_categoria);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_plaza, a.folio, a.id_categoria, a.id_area, a.fecha_creacion, a.status, a.observaciones,
                  b.id_area, b.area, b.titular, b.tel�fono, b.status, b.id_nivel, b.id_depende, b.orden,
                  c.id_categoria, c.categoria, c.status
                FROM admtbl_plantilla_plazas a 
                 LEFT JOIN admcat_areas b ON a.id_area=b.id_area
                 LEFT JOIN admcat_categorias c ON a.id_categoria=c.id_categoria";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_plaza' => $data['id_plaza'],
                               'folio' => $data['folio'],
                               'id_categoria' => $data['id_categoria'],
                               'id_area' => $data['id_area'],
                               'fecha_creacion' => $data['fecha_creacion'],
                               'status' => $data['status'],
                               'observaciones' => $data['observaciones'],
                               'area' => $data['area'],
                               'titular_area' => $data['titular'],
                               'tel�fono' => $data['tel�fono'],
                               'status_area' => $data['status'],
                               'id_nivel_area' => $data['id_nivel'],
                               'id_depende_area' => $data['id_depende'],
                               'orden_area' => $data['orden'],
                               'categoria' => $data['categoria']
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_plantilla_plazas(id_plaza, folio, id_categoria, id_area, fecha_creacion, status, observaciones)
                VALUES(:id_plaza, :folio, :id_categoria, :id_area, :fecha_creacion, :status, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_plaza" => $this->id_plaza, ":folio" => $this->folio, ":id_categoria" => $this->id_categoria, ":id_area" => $this->id_area, ":fecha_creacion" => $this->fecha_creacion, ":status" => $this->status, ":observaciones" => $this->observaciones));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_plantilla_plazas
                   SET folio=:folio, id_categoria=:id_categoria, id_area=:id_area, fecha_creacion=:fecha_creacion, status=:status, observaciones=:observaciones
                WHERE id_plaza=:id_plaza;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_plaza" => $this->id_plaza, ":folio" => $this->folio, ":id_categoria" => $this->id_categoria, ":id_area" => $this->id_area, ":fecha_creacion" => $this->fecha_creacion, ":status" => $this->status, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>