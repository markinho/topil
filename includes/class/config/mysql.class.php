<?php
/**
 * @author SisA
 * @copyright 2013
 */
require_once('configbd.class.php');

class MySqlPdo extends PDO
{
    public function __construct(){
        $dsn = 'mysql:host=' . ConfigBD::read('db.host') . ';dbname=' . ConfigBD::read('db.dbname');
        $user = ConfigBD::read('db.user');
        $pswd = ConfigBD::read('db.pswd');

        try{
            parent::__construct($dsn, $user, $pswd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",PDO::ATTR_PERSISTENT => true));
            parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $e){
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function getXsist($dt){
        return ConfigBD::getXsist($dt);
    }
}
?>