<?php
/**
 *
 */
class LogtblEqAlta
{
    public $id_registro; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_equipamento; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $oficio; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $asunto; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_folio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_alta; /** @Tipo: timestamp, @Acepta Nulos: NO, @Llave: --, @Default: CURRENT_TIMESTAMP */
    public $id_usuario; /** @Tipo: smallint(5), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblEquipamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_equipamento.class.php';
        $this->LogtblEquipamento = new LogtblEquipamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_registro)
    {
        $sql = "SELECT id_registro, id_equipamento, oficio, fecha_oficio, asunto, fecha_folio, fecha_alta, id_usuario
                FROM logtbl_eq_alta
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_registro' => $id_registro));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_registro = $data['id_registro'];
            $this->id_equipamento = $data['id_equipamento'];
            $this->oficio = $data['oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->asunto = $data['asunto'];
            $this->fecha_folio = $data['fecha_folio'];
            $this->fecha_alta = $data['fecha_alta'];
            $this->id_usuario = $data['id_usuario'];

            $this->LogtblEquipamento->select($this->id_equipamento);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_registro, a.id_equipamento, a.oficio, a.fecha_oficio, a.asunto, a.fecha_folio, a.fecha_alta, a.id_usuario,
                  b.id_equipamento, b.id_marca, b.id_tipo, b.id_entidad, b.id_dependencia, b.inventario, b.serie, b.modelo, b.id_estatus
                FROM logtbl_eq_alta a 
                 LEFT JOIN logtbl_equipamento b ON a.id_equipamento=b.id_equipamento";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_registro' => $data['id_registro'],
                               'id_equipamento' => $data['id_equipamento'],
                               'oficio' => $data['oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'asunto' => $data['asunto'],
                               'fecha_folio' => $data['fecha_folio'],
                               'fecha_alta' => $data['fecha_alta'],
                               'id_usuario' => $data['id_usuario'],
                               'logtbl_equipamento_id_marca' => $data['id_marca'],
                               'logtbl_equipamento_id_tipo' => $data['id_tipo'],
                               'logtbl_equipamento_id_entidad' => $data['id_entidad'],
                               'logtbl_equipamento_id_dependencia' => $data['id_dependencia'],
                               'logtbl_equipamento_inventario' => $data['inventario'],
                               'logtbl_equipamento_serie' => $data['serie'],
                               'logtbl_equipamento_modelo' => $data['modelo'],
                               'logtbl_equipamento_id_estatus' => $data['id_estatus'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_eq_alta(id_equipamento, id_usuario)
                VALUES(:id_equipamento, :id_usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_equipamento" => $this->id_equipamento, ":id_usuario" => $this->id_usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_eq_alta
                   SET id_equipamento=:id_equipamento, oficio=:oficio, fecha_oficio=:fecha_oficio, asunto=:asunto, fecha_folio=:fecha_folio, fecha_alta=:fecha_alta, id_usuario=:id_usuario
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_registro" => $this->id_registro, ":id_equipamento" => $this->id_equipamento, ":oficio" => $this->oficio, ":fecha_oficio" => $this->fecha_oficio, ":asunto" => $this->asunto, ":fecha_folio" => $this->fecha_folio, ":fecha_alta" => $this->fecha_alta, ":id_usuario" => $this->id_usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>