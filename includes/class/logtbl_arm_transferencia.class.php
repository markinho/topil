<?php
/**
 *
 */
class LogtblArmTransferencia
{
    public $id_transaccion; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $n_transaccion; /** @Tipo: varchar(20), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $matricula; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha; /** @Tipo: timestamp, @Acepta Nulos: NO, @Llave: --, @Default: CURRENT_TIMESTAMP */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblArmArmamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_arm_armamento.class.php';
        $this->LogtblArmArmamento = new LogtblArmArmamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_transaccion)
    {
        $sql = "SELECT id_transaccion, n_transaccion, matricula, fecha
                FROM logtbl_arm_transferencia
                WHERE id_transaccion=:id_transaccion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_transaccion' => $id_transaccion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_transaccion = $data['id_transaccion'];
            $this->n_transaccion = $data['n_transaccion'];
            $this->matricula = $data['matricula'];
            $this->fecha = $data['fecha'];

            $this->LogtblArmArmamento->select($this->matricula);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_transaccion, a.n_transaccion, a.matricula, a.fecha,
                  b.matricula, b.serie, b.id_loc, b.id_modelo, b.id_foliod, b.id_situacion, b.id_propietario, b.id_instituciones, b.id_estado, b.id_estatus, b.id_motivo_movimiento, b.id_ubicacion, b.id_corporacion
                FROM logtbl_arm_transferencia a 
                 LEFT JOIN logtbl_arm_armamento b ON a.matricula=b.matricula";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_transaccion' => $data['id_transaccion'],
                               'n_transaccion' => $data['n_transaccion'],
                               'matricula' => $data['matricula'],
                               'fecha' => $data['fecha'],
                               'logtbl_arm_armamento_serie' => $data['serie'],
                               'logtbl_arm_armamento_id_loc' => $data['id_loc'],
                               'logtbl_arm_armamento_id_modelo' => $data['id_modelo'],
                               'logtbl_arm_armamento_id_foliod' => $data['id_foliod'],
                               'logtbl_arm_armamento_id_situacion' => $data['id_situacion'],
                               'logtbl_arm_armamento_id_propietario' => $data['id_propietario'],
                               'logtbl_arm_armamento_id_instituciones' => $data['id_instituciones'],
                               'logtbl_arm_armamento_id_estado' => $data['id_estado'],
                               'logtbl_arm_armamento_id_estatus' => $data['id_estatus'],
                               'logtbl_arm_armamento_id_motivo_movimiento' => $data['id_motivo_movimiento'],
                               'logtbl_arm_armamento_id_ubicacion' => $data['id_ubicacion'],
                               'logtbl_arm_armamento_id_corporacion' => $data['id_corporacion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_transferencia(id_transaccion, n_transaccion, matricula, fecha)
                VALUES(:id_transaccion, :n_transaccion, :matricula, :fecha);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_transaccion" => $this->id_transaccion, ":n_transaccion" => $this->n_transaccion, ":matricula" => $this->matricula, ":fecha" => $this->fecha));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_transferencia
                   SET n_transaccion=:n_transaccion, matricula=:matricula, fecha=:fecha
                WHERE id_transaccion=:id_transaccion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_transaccion" => $this->id_transaccion, ":n_transaccion" => $this->n_transaccion, ":matricula" => $this->matricula, ":fecha" => $this->fecha));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>