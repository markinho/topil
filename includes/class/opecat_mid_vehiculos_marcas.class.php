<?php
/**
 *
 */
class OpecatMidVehiculosMarcas
{
    public $id_vehiculo_marca; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $vehiculo_marca; /** @Tipo: varchar(80), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }


     /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Vehiculos_Marcas($id=0){
        $aryDatos = $this->selectAll('id_vehiculo_marca Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_vehiculo_marca"] )
                $html .= '<option value="'.$datos["id_vehiculo_marca"].'" selected>'.$datos["vehiculo_marca"].'</option>';
            else
                $html .= '<option value="'.$datos["id_vehiculo_marca"].'" >'.$datos["vehiculo_marca"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_vehiculo_marca)
    {
        $sql = "SELECT id_vehiculo_marca, vehiculo_marca
                FROM opecat_mid_vehiculos_marcas
                WHERE id_vehiculo_marca=:id_vehiculo_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo_marca' => $id_vehiculo_marca));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo_marca = $data['id_vehiculo_marca'];
            $this->vehiculo_marca = $data['vehiculo_marca'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sql_order="", $sql_limit="")
    {
        $sql = "SELECT a.id_vehiculo_marca, a.vehiculo_marca
                FROM opecat_mid_vehiculos_marcas a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo_marca' => $data['id_vehiculo_marca'],
                               'vehiculo_marca' => $data['vehiculo_marca'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_vehiculos_marcas(id_vehiculo_marca, vehiculo_marca)
                VALUES(:id_vehiculo_marca, :vehiculo_marca);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo_marca" => $this->id_vehiculo_marca, ":vehiculo_marca" => $this->vehiculo_marca));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_vehiculos_marcas
                   SET vehiculo_marca=:vehiculo_marca
                WHERE id_vehiculo_marca=:id_vehiculo_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo_marca" => $this->id_vehiculo_marca, ":vehiculo_marca" => $this->vehiculo_marca));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>