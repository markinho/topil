<?php
/**
 *
 */
class LogcatArmFoliod
{
    public $id_foliod; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $foliod; /** @Tipo: varchar(8), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_folioc; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatArmFolioc; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_arm_folioc.class.php';
        $this->LogcatArmFolioc = new LogcatArmFolioc();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_foliod)
    {
        $sql = "SELECT id_foliod, foliod, id_folioc, xstat
                FROM logcat_arm_foliod
                WHERE id_foliod=:id_foliod;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_foliod' => $id_foliod));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_foliod = $data['id_foliod'];
            $this->foliod = $data['foliod'];
            $this->id_folioc = $data['id_folioc'];
            $this->xstat = $data['xstat'];

            $this->LogcatArmFolioc->select($this->id_folioc);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para mostrar la lista de folios dentro de un combobox.
     * @param int $id, id del foliod seleccionado por deafult     
     * @return array html(options)
     */
    public function shwFoliod( $id=0, $id_folioc ){
        if( $id_folioc != 0 )
            $aryDatos = $this->selectAll('b.id_folioc=' . $id_folioc, 'b.id_folioc Asc');
        else
            $aryDatos = $this->selectAll('a.xstat=1', 'a.foliod Asc');
        
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_foliod"] )
                $html .= '<option value="'.$datos["id_foliod"].'" selected>'.$datos["foliod"].'</option>';
            else
                $html .= '<option value="'.$datos["id_foliod"].'" >'.$datos["foliod"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener el id de la clase del arma atravez de la tabla de marcas
     * @param  $int id_marca, se recibe el parametro id_marca para hacer el filtro
     * @return el id de la clase
     */
    public function getIdFolioc( $id_foliod ){
	
		$sql = "SELECT fc.id_folioc
                FROM logcat_arm_folioc as fc
				INNER JOIN logcat_arm_foliod as fd On fc.id_folioc=fd.id_folioc
				WHERE fd.id_foliod=:id_foliod
				AND fc.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_foliod' => $id_foliod, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_folioc'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_foliod, a.foliod, a.id_folioc, a.xstat,
                  b.id_folioc, b.folioc
                FROM logcat_arm_foliod a 
                 LEFT JOIN logcat_arm_folioc b ON a.id_folioc=b.id_folioc";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_foliod' => $data['id_foliod'],
                               'foliod' => $data['foliod'],
                               'id_folioc' => $data['id_folioc'],
                               'xstat' => $data['xstat'],
                               'logcat_arm_folioc_folioc' => $data['folioc'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_foliod(id_foliod, foliod, id_folioc, xstat)
                VALUES(:id_foliod, :foliod, :id_folioc, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_foliod" => $this->id_foliod, ":foliod" => $this->foliod, ":id_folioc" => $this->id_folioc, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_foliod
                   SET foliod=:foliod, id_folioc=:id_folioc, xstat=:xstat
                WHERE id_foliod=:id_foliod;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_foliod" => $this->id_foliod, ":foliod" => $this->foliod, ":id_folioc" => $this->id_folioc, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>