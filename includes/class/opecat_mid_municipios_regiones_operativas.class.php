<?php
/**
 *
 */
class OpecatMidMunicipiosRegionesOperativas
{
    public $id_estado; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_region; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidRegionesOperativas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_regiones_operativas.class.php';
        $this->OpecatMidRegionesOperativas = new OpecatMidRegionesOperativas();
    }

    /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Municipios_Regiones_Operativas($id=0){
        $aryDatos = $this->selectAll('id_region Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_region"] )
                $html .= '<option value="'.$datos["id_region"].'" selected>'.$datos["id_region"].'</option>';
            else
                $html .= '<option value="'.$datos["id_region"].'" >'.$datos["id_region"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_estado, $id_municipio)
    {
        $sql = "SELECT id_estado, id_municipio, id_region
                FROM opecat_mid_municipios_regiones_operativas
                WHERE id_estado=:id_estado AND id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_estado' => $id_estado, ':id_municipio' => $id_municipio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_estado = $data['id_estado'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_region = $data['id_region'];

            $this->OpecatMidRegionesOperativas->select($this->id_region);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_estado, a.id_municipio,  a.id_region,
                  b.id_region, b.region
                FROM opecat_mid_municipios_regiones_operativas a
                 LEFT JOIN opecat_mid_regiones_operativas b ON a.id_region=b.id_region";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_estado' => $data['id_estado'],
                               'id_municipio' => $data['id_municipio'],
                               'id_region' => $data['id_region'],
                               'opecat_mid_regiones_operativas_region' => $data['region'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_municipios_regiones_operativas(id_estado, id_municipio, id_region)
                VALUES(:id_estado, :id_municipio, :id_region);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estado" => $this->id_estado, ":id_municipio" => $this->id_municipio, ":id_region" => $this->id_region));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_municipios_regiones_operativas
                   SET id_region=:id_region
                WHERE id_estado=:id_estado AND id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estado" => $this->id_estado, ":id_municipio" => $this->id_municipio, ":id_region" => $this->id_region));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>