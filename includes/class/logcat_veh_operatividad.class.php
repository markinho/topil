<?php
/**
 *
 */
class LogcatVehOperatividad
{
    public $id_operatividad; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $operatividad; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de operatividad dentro de un combobox.
     * @param int $id, id de operatividad seleccionado por deafult  
     * @param int $id_entidad, id de operatividad para el filtro de operatividad
     * @return array html(options)
     */
    public function shwOperatividad($id=0){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.operatividad Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_operatividad"] )
                $html .= '<option value="'.$datos["id_operatividad"].'" selected>'.$datos["operatividad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_operatividad"].'" >'.$datos["operatividad"].'</option>';
        }
        return $html;
    }
	
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_operatividad)
    {
        $sql = "SELECT id_operatividad, operatividad, xstat
                FROM logcat_veh_operatividad
                WHERE id_operatividad=:id_operatividad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_operatividad' => $id_operatividad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_operatividad = $data['id_operatividad'];
            $this->operatividad = $data['operatividad'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_operatividad, a.operatividad, a.xstat
                FROM logcat_veh_operatividad a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_operatividad' => $data['id_operatividad'],
                               'operatividad' => $data['operatividad'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_operatividad(id_operatividad, operatividad, xstat)
                VALUES(:id_operatividad, :operatividad, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_operatividad" => $this->id_operatividad, ":operatividad" => $this->operatividad, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_operatividad
                   SET operatividad=:operatividad, xstat=:xstat
                WHERE id_operatividad=:id_operatividad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_operatividad" => $this->id_operatividad, ":operatividad" => $this->operatividad, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>