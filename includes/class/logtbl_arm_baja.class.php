<?php
/**
 *
 */
class LogtblArmBaja
{
    public $id_baja; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $matricula; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $oficio; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $motivo; /** @Tipo: tinyint(4), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_baja; /** @Tipo: timestamp, @Acepta Nulos: NO, @Llave: --, @Default: CURRENT_TIMESTAMP */
    public $usuario; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblArmArmamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_arm_armamento.class.php';
        $this->LogtblArmArmamento = new LogtblArmArmamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_baja)
    {
        $sql = "SELECT id_baja, matricula, oficio, fecha_oficio, motivo, fecha_baja, usuario
                FROM logtbl_arm_baja
                WHERE id_baja=:id_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_baja' => $id_baja));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_baja = $data['id_baja'];
            $this->matricula = $data['matricula'];
            $this->oficio = $data['oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->motivo = $data['motivo'];
            $this->fecha_baja = $data['fecha_baja'];
            $this->usuario = $data['usuario'];

            $this->LogtblArmArmamento->select($this->matricula);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_baja, a.matricula, a.oficio, a.fecha_oficio, a.motivo, a.fecha_baja, a.usuario,
                  b.matricula, b.serie, b.id_loc, b.id_marca, b.id_foliod, b.id_situacion, b.id_propietario, b.id_estado, b.id_estatus, b.id_motivo_movimiento, b.id_tipo_funcionamiento
                FROM logtbl_arm_baja a 
                 LEFT JOIN logtbl_arm_armamento b ON a.matricula=b.matricula";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_baja' => $data['id_baja'],
                               'matricula' => $data['matricula'],
                               'oficio' => $data['oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'motivo' => $data['motivo'],
                               'fecha_baja' => $data['fecha_baja'],
                               'usuario' => $data['usuario'],
                               'logtbl_arm_armamento_serie' => $data['serie'],
                               'logtbl_arm_armamento_id_loc' => $data['id_loc'],
                               'logtbl_arm_armamento_id_marca' => $data['id_marca'],
                               'logtbl_arm_armamento_id_foliod' => $data['id_foliod'],
                               'logtbl_arm_armamento_id_situacion' => $data['id_situacion'],
                               'logtbl_arm_armamento_id_propietario' => $data['id_propietario'],
                               'logtbl_arm_armamento_id_estado' => $data['id_estado'],
                               'logtbl_arm_armamento_id_estatus' => $data['id_estatus'],
                               'logtbl_arm_armamento_id_motivo_movimiento' => $data['id_motivo_movimiento'],
                               'logtbl_arm_armamento_id_tipo_funcionamiento' => $data['id_tipo_funcionamiento'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


    public function selectLstBajas($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "select matricula, serie, marca, modelo, calibre, fecha_baja from viewarmlstbajas ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";        
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'matricula' => $data['matricula'],
                               'serie' => $data['serie'],
                               'marca' => $data['marca'],
                               'modelo' => $data['modelo'],
                               'calibre' => $data['calibre'],
                               'fecha_baja' => $data['fecha_baja'],                              
                               );
            }            
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_baja(matricula,
                                            oficio,
                                            fecha_oficio,
                                            motivo,
                                            usuario)
                                    VALUES(:matricula,
                                            :oficio,
                                            :fecha_oficio,
                                            :motivo,
                                            :usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, 
                                ":oficio" => $this->oficio, 
                                ":fecha_oficio" => $this->fecha_oficio, 
                                ":motivo" => $this->motivo,                                 
                                ":usuario" => $this->usuario));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_baja
                   SET matricula=:matricula, 
                        oficio=:oficio, 
                        fecha_oficio=:fecha_oficio, 
                        motivo=:motivo, 
                        fecha_baja=:fecha_baja, 
                        usuario=:usuario
                WHERE id_baja=:id_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_baja" => $this->id_baja, 
                                ":matricula" => $this->matricula, 
                                ":oficio" => $this->oficio, 
                                ":fecha_oficio" => $this->fecha_oficio, 
                                ":motivo" => $this->motivo, 
                                ":fecha_baja" => $this->fecha_baja, 
                                ":usuario" => $this->usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar datos de la tabla de logtbl_arm_armamento en el proceso de baja
     * @return boolean true si el proceso es satisfactorio
     */
    public function update_baja()
    {
        $sql = "UPDATE logtbl_arm_armamento
                   SET id_situacion=:id_situacion, 
                   id_estado=:id_estado, 
                   id_estatus=:id_estatus, 
                   id_motivo_movimiento=:id_motivo_movimiento
                WHERE matricula=:matricula;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            // se pone con situacion = 2 como baja en la tabla principal de armamento 
            //y se agrega el registro con datos de baja en la tabla de logtbl_arm_baja
            $qry->execute(array(":matricula" => $this->matricula, 
                                ":id_situacion" => 2, 
                                ":id_estado" => 1, 
                                ":id_estatus" => 6, 
                                ":id_motivo_movimiento" => 7));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>