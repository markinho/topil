<?php
/**
 *
 */
class LogcatVehModelo
{
    public $id_modelo; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $modelo; /** @Tipo: varchar(4), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de modelos dentro de un combobox.
     * @param int $id, id del modelo seleccionado por deafult     
     * @return array html(options)
     */
    public function shwModelo($id=0){
        $aryDatos = $this->selectAll('xstat=1', 'modelo Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_modelo"] )
                $html .= '<option value="'.$datos["id_modelo"].'" selected>'.$datos["modelo"].'</option>';
            else
                $html .= '<option value="'.$datos["id_modelo"].'" >'.$datos["modelo"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_modelo)
    {
        $sql = "SELECT id_modelo, modelo, xstat
                FROM logcat_veh_modelo
                WHERE id_modelo=:id_modelo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_modelo' => $id_modelo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_modelo = $data['id_modelo'];
            $this->modelo = $data['modelo'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_modelo, a.modelo, a.xstat
                FROM logcat_veh_modelo a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_modelo' => $data['id_modelo'],
                               'modelo' => $data['modelo'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_modelo(id_modelo, modelo, xstat)
                VALUES(:id_modelo, :modelo, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_modelo" => $this->id_modelo, ":modelo" => $this->modelo, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_modelo
                   SET modelo=:modelo, xstat=:xstat
                WHERE id_modelo=:id_modelo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_modelo" => $this->id_modelo, ":modelo" => $this->modelo, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>