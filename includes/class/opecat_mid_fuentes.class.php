<?php
/**
 *
 */
class OpecatMidFuentes
{
    public $id_fuente; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fuente; /** @Tipo: varchar(200), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Fuentes($id=0){
        $aryDatos = $this->selectAll('id_fuente Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_fuente"] )
                $html .= '<option value="'.$datos["id_fuente"].'" selected>'.$datos["fuente"].'</option>';
            else
                $html .= '<option value="'.$datos["id_fuente"].'" >'.$datos["fuente"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_fuente)
    {
        $sql = "SELECT id_fuente, fuente
                FROM opecat_mid_fuentes
                WHERE id_fuente=:id_fuente;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_fuente' => $id_fuente));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_fuente = $data['id_fuente'];
            $this->fuente = $data['fuente'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_fuente, a.fuente
                FROM opecat_mid_fuentes a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_fuente' => $data['id_fuente'],
                               'fuente' => $data['fuente'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_fuentes(id_fuente, fuente)
                VALUES(:id_fuente, :fuente);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_fuente" => $this->id_fuente, ":fuente" => $this->fuente));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_fuentes
                   SET fuente=:fuente
                WHERE id_fuente=:id_fuente;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_fuente" => $this->id_fuente, ":fuente" => $this->fuente));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>