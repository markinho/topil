<?php
/**
 *
 */
class Xtblpermisos
{
    public $id_permiso_usr; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_permiso; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_usuario; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $tipo; /** @Tipo: tinyint, @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    
    public $msjError; // almacena el mensaje de error si éste ocurre
    private $_conexBD; // objeto de conexión a la base de datos
    public $Xcatpermisos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $Xtblusuarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xcatpermisos.class.php';
        require_once 'xtblusuarios.class.php';
        $this->Xcatpermisos = new Xcatpermisos();
        $this->Xtblusuarios = new Xtblusuarios();
    }

    /**
     * Función para obtener un registro específico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realizó con éxito
     */
    public function select($id_permiso_usr)
    {
        $sql = "SELECT id_permiso_usr, id_permiso, id_usuario, tipo
                FROM xtblpermisos
                WHERE id_permiso_usr=:id_permiso_usr;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_permiso_usr' => $id_permiso_usr));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_permiso_usr = $data['id_permiso_usr'];
            $this->id_permiso = $data['id_permiso'];
            $this->id_usuario = $data['id_usuario'];
            $this->tipo = $data['tipo'];

            $this->Xcatpermisos->select($this->id_permiso);
            $this->Xtblusuarios->select($this->id_usuario);

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selección de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_permiso_usr, a.id_permiso, a.id_usuario, a.tipo AS tipo_perm,
                    b.permiso, b.descripcion, b.tipo, b.stat,
                    c.nom_usr, c.nombre, c.id_perfil, c.fecha_reg, c.fecha_edit, c.stat
                FROM xtblpermisos a 
                    LEFT JOIN xcatpermisos b ON a.id_permiso=b.id_permiso
                    LEFT JOIN xtblusuarios c ON a.id_usuario=c.id_usuario";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_permiso_usr' => $data['id_permiso_usr'],
                               'id_permiso' => $data['id_permiso'],
                               'id_usuario' => $data['id_usuario'],
                               'tipo_perm' => ($data['tipo_perm'] == 1) ? 'Permitido' : 'Restringido',
                               'xcatpermisos_permiso' => $data['permiso'],
                               'xcatpermisos_descripcion' => utf8_encode($data['descripcion']),
                               'xcatpermisos_tipo' => $data['tipo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el último id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xtblpermisos(id_permiso, id_usuario, tipo)
                VALUES(:id_permiso, :id_usuario, :tipo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permiso" => $this->id_permiso, ":id_usuario" => $this->id_usuario, ":tipo" => $this->tipo));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Función para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xtblpermisos
                   SET permiso=:permiso, id_usuario=:id_usuario, tipo=:tipo
                WHERE id_permiso_usr=:id_permiso_usr;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permiso_usr" => $this->id_permiso_usr, ":permiso" => $this->permiso, ":id_usuario" => $this->id_usuario, ":tipo" => $this->tipo));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {
        $sql = "DELETE FROM xtblpermisos WHERE id_permiso_usr=:id_permiso_usr;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permiso_usr" => $this->id_permiso_usr));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

}


?>