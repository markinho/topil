<?php
/**
 *
 */
class LogcatVehTipo
{
    public $id_tipo; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo; /** @Tipo: varchar(75), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_tipo_sn; /** @Tipo: int(5), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $id_marca; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_clasificacion; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatVehMarca; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehClasificacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_veh_marca.class.php';
        require_once 'logcat_veh_clasificacion.class.php';
        $this->LogcatVehMarca = new LogcatVehMarca();
        $this->LogcatVehClasificacion = new LogcatVehClasificacion();
    }

	/**
     * Funci�n para mostrar la lista de tipos dentro de un combobox.
     * @param int $id, id del tipo seleccionado por deafult  
     * @param int $id_entidad, id del tipo para el filtro de tipos
     * @return array html(options)
     */
    public function shwTipos($id=0,$id_clasificacion=0,$id_marca=0){
		if( $id_clasificacion != 0 ){
			$filtros.= 'a.id_clasificacion=' . $id_clasificacion . ' and '; 	
		}
		if( $id_marca != 0 ){
			$filtros.= 'a.id_marca=' . $id_marca . ' and '; 	
		}
	    $aryDatos = $this->selectAll( $filtros . ' a.xstat=1', 'a.tipo Asc' );
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo"] )
                $html .= '<option value="'.$datos["id_tipo"].'" selected>'.$datos["tipo"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo"].'" >'.$datos["tipo"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo)
    {
        $sql = "SELECT id_tipo, tipo, id_tipo_sn, xstat, id_marca, id_clasificacion
                FROM logcat_veh_tipo
                WHERE id_tipo=:id_tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo' => $id_tipo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo = $data['id_tipo'];
            $this->tipo = $data['tipo'];
            $this->id_tipo_sn = $data['id_tipo_sn'];
            $this->xstat = $data['xstat'];
            $this->id_marca = $data['id_marca'];
            $this->id_clasificacion = $data['id_clasificacion'];

            $this->LogcatVehMarca->select($this->id_marca);
            $this->LogcatVehClasificacion->select($this->id_clasificacion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo, a.tipo, a.id_tipo_sn, a.xstat, a.id_marca, a.id_clasificacion,
                  b.id_marca, b.marca, b.id_marca_sn, b.xstat,
                  c.id_clasificacion, c.clasificacion, c.xstat
                FROM logcat_veh_tipo a 
                 LEFT JOIN logcat_veh_marca b ON a.id_marca=b.id_marca
                 LEFT JOIN logcat_veh_clasificacion c ON a.id_clasificacion=c.id_clasificacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo' => $data['id_tipo'],
                               'tipo' => $data['tipo'],
                               'id_tipo_sn' => $data['id_tipo_sn'],
                               'xstat' => $data['xstat'],
                               'id_marca' => $data['id_marca'],
                               'id_clasificacion' => $data['id_clasificacion'],
                               'logcat_veh_marca_marca' => $data['marca'],
                               'logcat_veh_marca_id_marca_sn' => $data['id_marca_sn'],
                               'logcat_veh_marca_xstat' => $data['xstat'],
                               'logcat_veh_clasificacion_clasificacion' => $data['clasificacion'],
                               'logcat_veh_clasificacion_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_tipo(id_tipo, tipo, id_tipo_sn, xstat, id_marca, id_clasificacion)
                VALUES(:id_tipo, :tipo, :id_tipo_sn, :xstat, :id_marca, :id_clasificacion);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo" => $this->id_tipo, ":tipo" => $this->tipo, ":id_tipo_sn" => $this->id_tipo_sn, ":xstat" => $this->xstat, ":id_marca" => $this->id_marca, ":id_clasificacion" => $this->id_clasificacion));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_tipo
                   SET tipo=:tipo, id_tipo_sn=:id_tipo_sn, xstat=:xstat, id_marca=:id_marca, id_clasificacion=:id_clasificacion
                WHERE id_tipo=:id_tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo" => $this->id_tipo, ":tipo" => $this->tipo, ":id_tipo_sn" => $this->id_tipo_sn, ":xstat" => $this->xstat, ":id_marca" => $this->id_marca, ":id_clasificacion" => $this->id_clasificacion));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>