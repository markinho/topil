<?php
/**
 * Complemento del llamado ajax para listar las clases dentro de un combobox. 
 * @param int id_tipo, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/logcat_arm_clase.class.php';
    $objCla = new LogcatArmClase();
    
    $datos = '<option value="0"></option>';
    if ($_GET["id_tipo"] != 0){
        $datos .= $objCla->shwClases( 0, $_GET["id_tipo"] );
    }
    if (empty($objCla->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objCla->msjError;        
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>