<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_arm_armamento.class.php';    
    $objSys = new System();
        
    $objDatos = new LogtblArmArmamento();    

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    
    if (!empty($_GET["txtSearch"])) {
        $sql_where = ' (a.matricula LIKE ? )';
        $sql_values = array('%' . $_GET['txtSearch'] .'%');
    }
    
    // Ordenaci�n...
    $sql_order = '';
    
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'a.matricula ' . $_GET['typeSort'];
    } 
    
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
      
    $datos = $objDatos->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    $xDat = $objDatos->LogcatArmMarca;
    if ( $totalReg > 0 ) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["matricula"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["matricula"] . '">';       			                    			                               
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["matricula"] . '</td>';                                                
                $html .= '<td style="text-align: center; width:  6%;">' . $xDat->LogcatArmClase->LogcatArmTipo->getTipo( $dato["id_marca"] ) . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $xDat->getMarca( $dato["id_marca"] ) . '</td>';
                $html .= '<td style="text-align: center; width: 20%;">' . $xDat->LogcatArmModelo->getModelo( $dato["id_marca"] ) . '</td>';
                $html .= '<td style="text-align: center; width: 19%;">' . $xDat->LogcatArmClase->getClase( $dato["id_marca"] ) . '</td>';  
                $html .= '<td style="text-align: center; width: 20%;">' . $xDat->LogcatArmModelo->LogcatArmCalibre->getCalibre( $dato["id_marca"] ) . '</td>';                              
                $html .= '<td style="text-align: center; width:  5%;">';
$html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["matricula"] . '" title="Asignar esta arma..."><img src="' . PATH_IMAGES . 'icons/go_left24.png" alt="selec" /></a>';
                $html .='</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';               		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron matriculas en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>