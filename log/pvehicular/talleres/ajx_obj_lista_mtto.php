<div id="dvList-Prod" style="border: none; min-width: 980px; margin: auto auto; margin-top: 5px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td style="color: #153e7e; font-size: 14pt; font-weight: bold; padding: 7px 2px 7px 2px; text-align: center;">LISTA  DE  PRODUCTOS</td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr style="height: 30px;">
                    <th style="width: 7%;">&nbsp;</th>
                    <th style="font-size: 11pt; width: 6%;">N.P.</th> 
                    <th style="font-size: 11pt; width: 10%;">MODO</th>
                    <th style="font-size: 11pt; width: 15%;">COD. BAR / CLAVE</th>
                    <th style="font-size: 11pt; width: 35%;">PRODUCTO</th>
                    <th style="font-size: 11pt; width: 9%;">PRECIO UNIT.</th>                       
                    <th style="font-size: 11pt; width: 8%;">CANT.</th>                   
                    <th style="font-size: 11pt; width: 10%;">IMPORTE</th>
                </tr>
            </table>
        </div>
        <div id="dvList-Prod-Body" class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 300px; overflow-y: hidden;">
            <?php
            $totalImporte = 0;
            $datos = $venta->getProdDespach($usr->xIdEmpleado, date("Y-m-d"));
            if( count($datos["datos"]) ){
                $html = '<table class="xGrid-tbBody" id="tbList-Prod">';
                foreach( $datos["datos"] As $reg => $dato ){
                    $id_crypt = $sys->sisa_encrypt($dato["ID_PRODUCTO"]);
                    $html .= '<tr>';
               			//--------------------- Impresion de datos ----------------------//
                        $html .= '<td style="text-align: center; width: 7%;">';                
                        $html .= '  <a href="#" rel="dlt-' . $id_crypt . '" class="lnkBtnOpcionGrid" title="Excluir..." ><img src="includes/css/imgs/icons/delete24.png" alt="delete" style="height: 16px; margin-top: 5px; width: 16px;" /></a>';
                        $html .= '</td>';
                        $html .= '<td style="font-size: 11pt; text-align: center; width: 6%;">' . ($reg+1) . '</td>';
                        $modo = ( !empty($dato["ID_PEDIDO"]) ) ? "POR PEDIDO" : "VENTA";
                        $html .= '<td style="font-size: 11pt; text-align: center; width: 10%;">' . $modo . '</td>';
                        $codigo = ( !empty($dato["COD_BARRAS"]) ) ? $dato["COD_BARRAS"] : $dato["CLAVE"];
                        $html .= '<td style="font-size: 11pt; text-align: center; width: 15%;">' . $codigo . '</td>';
                        $html .= '<td style="font-size: 11pt; text-align: left; width: 35%;">' . $dato["PRODUCTO"] . '</td>';                
                        $html .= '<td style="font-size: 11pt; text-align: right; width: 9%;">$ ' . number_format($dato["PRECIO"], 2, ".", ",") . '</td>';                
                        $html .= '<td style="font-size: 11pt; text-align: center; width: 8%;">' . $dato["CANTIDAD"] . '</td>';
                        $html .= '<td style="font-size: 11pt; text-align: right; width: 10%;">$ ' . number_format($dato["IMPORTE"], 2, ".", ",") . '</td>';
                     	//---------------------------------------------------------------//
               		$html .= '</tr>';  		
                }
                $html .= '</table>';
                
                echo $html;
                $totalImporte = $datos["total"];
            }
            ?>
        </div>
    </div>