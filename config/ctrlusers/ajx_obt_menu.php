<?php
/**
 * Complemento ajax para obtener el menú de arbol. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/xtblmenu.class.php';
    include $path . 'includes/class/xcatperfiles.class.php';
    $objMenu = new Xtblmenu();
    $objPerfil = new Xcatperfiles();
    
    if ($menu_cont = $objMenu->getMenuTree()) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["menu"] = utf8_encode($menu_cont);
        $ajx_datos["perfil"] = utf8_encode( $objPerfil->shwPerfiles(0, "id_perfil > 0 AND tipo='General'") );
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objMenu->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesión...";
    echo json_encode($ajx_datos);
}
?>