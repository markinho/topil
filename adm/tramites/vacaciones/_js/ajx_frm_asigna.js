$(document).ready(function(){ 
    
    
  
    //-- Controla el bot�n para visualizar el formulario de busqueda de armas para portacion..
    
    
    
    $('#frmRegistro').validate({
        rules:{
            cbxAnioPer:{
                min: 1,
            },
            cbxPeriodo:{
                min: 1,
            },
            txtDias:{
                required:true,
                digits: true,
            },
            
            // Cambios de Adscripcion
            txtFechaInicio: 'required',
            txtFechaFin: 'required',
            rbnPeriodo: 'required',
                        
            
    	},
    	messages:{
           cbxAnioPer:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	   cbxPeriodo:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtDias:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                digits: '<span class="ValidateError" title="Este campo solo acepta numeros"></span>',
                    
            },
            
            txtFechaInicio: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaFin: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnPeriodo:  '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            
            
    	},
        
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    

          

});

