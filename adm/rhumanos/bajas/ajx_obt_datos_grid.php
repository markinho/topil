<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_bajas.class.php';
    $objSys = new System();
    $objBajas = new AdmtblBajas();

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where = '(b.nombre LIKE ? OR b.a_paterno LIKE ? OR b.a_materno LIKE ?) '
                   . 'OR a.curp LIKE ? '
                   . 'OR d.categoria LIKE ? '
                   . 'OR a.num_oficio LIKE ? '
                   . 'OR e.motivo LIKE ? '
                   . 'OR a.fecha_baja LIKE ?';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',);
    }
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'b.a_paterno ' . $_GET['typeSort']  . ', b.a_materno ' . $_GET['typeSort']  . ', b.nombre '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'a.curp ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'd.categoria ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'a.num_oficio ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'e.motivo ' . $_GET['typeSort']; 
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'a.fecha_baja ' . $_GET['typeSort'];
    }
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
       
    $datos = $objBajas->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $sty_color = ( $_SESSION["xIdBaja"] == $dato["id_baja"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_baja"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                   $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 6%; ' . $sty_color . '">' . $dato["id_baja"] . '</td>';
       			$nombrePersona = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' .$dato["nombre"];                                   
                $html .= '<td style="text-align: left; width: 15%;"><a href="#" class="toolTipTrigger" rel="' . $dato["curp"] . '">' . $nombrePersona . '</a></td>';
                $html .= '<td style="text-align: center; width: 14%; ' . $sty_color . '">' . $dato["curp"] . '</td>';                                
                $html .= '<td style="text-align: center; width: 14%; ' . $sty_color . '">' . $dato["categoria"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">' . $dato["num_oficio"] . '</td>';
                $html .= '<td style="text-align: left; width: 15%; ' . $sty_color . '">' . $dato["motivo"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_baja"])) . '</td>';
                $html .= '<td style="text-align: center; width: 12%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="edt;' . $dato["id_baja"] . '" title="Modificar registro de baja..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';                
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="pdf;' . $dato["id_baja"] . '" title="Oficio de la baja en formato PDF..." ><img src="' . PATH_IMAGES . 'icons/doc_pdf24.png" alt="oficio pdf" /></a>';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="wrd;' . $dato["id_baja"] . '" title="Oficio de la baja en Word..." ><img src="' . PATH_IMAGES . 'icons/doc_word24.png" alt="oficio word" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron personas en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>