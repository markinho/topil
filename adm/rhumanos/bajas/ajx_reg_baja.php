<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_bajas.class.php';
    include $path . 'includes/class/admtbl_datos_personales.class.php';
    require  $path . 'includes/class/config/mysql.class.php';    
    
    $objSys = new System();
    $objUsr = new Usuario();
    $conexBD = new MySQLPDO();
    $objBaja = new AdmtblBajas();
    $objDatPer = new AdmtblDatosPersonales();  
    
    $idBaja = $_POST["hdnIdBaja"];
    $objBaja->id_baja = $idBaja;
    $objBaja->curp = $_POST["txtCurp"];
    $objBaja->id_motivo = $_POST["cbxMotivo"];
    $objBaja->fecha_baja = $objSys->convertirFecha($_POST["txtFechaBaja"], "yyyy-mm-dd");
    $objBaja->num_oficio = $_POST["txtNumOficio"];
    $objBaja->fecha_oficio = $objSys->convertirFecha($_POST["txtFechaOficio"], "yyyy-mm-dd");
    $objBaja->id_plaza = NULL;//$_POST["hdnPlaza"];
    $objBaja->observaciones = utf8_decode($_POST["txtObservaciones"]);
    
    $conexBD->beginTransaction();
    $result = ($_POST["hdnTypeOper"] == 1) ? $objBaja->insert() : $objBaja->update();
    if ($result) {
        if( $_POST["hdnTypeOper"] == 1 ){
            $_SESSION["xIdBaja"] = $result;
            $objSys->registroLog($objUsr->idUsr, 'admtbl_bajas', $_SESSION["xIdBaja"], "Ins");
            if( $objDatPer->delete($_POST["txtCurp"]) ){              
                $objSys->registroLog($objUsr->idUsr, 'admtbl_datos_personales', $_POST["txtCurp"], "Dlt");
                $conexBD->commit();                
                $ajx_datos['rslt']  = true;
                $ajx_datos['error'] = '';
            } else {
                $conexBD->rollBack();
                $ajx_datos['rslt']  = false;
                $ajx_datos['error'] = $objDatPer->msjError;
            }
        } else {
            $_SESSION["xIdBaja"] = $idBaja;
            $objSys->registroLog($objUsr->idUsr, 'admtbl_bajas', $_SESSION["xIdBaja"], "Edt");
            $conexBD->commit();            
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        }
            
    } else {
        $conexBD->rollBack();
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objBaja->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>