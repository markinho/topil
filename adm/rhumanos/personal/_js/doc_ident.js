$(document).ready(function(){
    var url_dir_tmp = xDcrypt($('#hdnUrlDirTemp').val());
    var url_upload = xDcrypt($('#hdnUrlUpload').val());
    //-- Script para cargar la Fotograf�a de Perfil Izquierdo...
    new AjaxUpload('#dvFotoIzq', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoIzq").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoIzq").val(result.file);
                $("#dvFotoIzq").empty();
                $("#dvFotoIzq").append(imgHtml);
            }
        }	
    });
    //-- Script para cargar la Fotograf�a de Frente...
    new AjaxUpload('#dvFotoFte', {
        action: url_upload ,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoFte").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoFte").val(result.file);
                $("#dvFotoFte").empty();
                $("#dvFotoFte").append(imgHtml);
            }
        }	
    });
    //-- Script para cargar la Fotograf�a de Perfil Derecho...
    new AjaxUpload('#dvFotoDer', {
        action: url_upload,
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoDer").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoDer").val(result.file);
                $("#dvFotoDer").empty();
                $("#dvFotoDer").append(imgHtml);
            }
        }	
    });
    //-- Script para cargar la Firma...
    new AjaxUpload('#dvFirma', {
        action: url_upload,
        dataType: 'json',
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFirma").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 2px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){            
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFirma").val(result.file);
                $("#dvFirma").empty();
                $("#dvFirma").append(imgHtml);
            }
        }	
    });
    //-- Script para cargar la Huella dactilar del pulgar Izquierdo...
    new AjaxUpload('#dvHuellaIzq', {
        action: url_upload,
        dataType: 'json',
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvHuellaIzq").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){            
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnHuellaIzq").val(result.file);
                $("#dvHuellaIzq").empty();
                $("#dvHuellaIzq").append(imgHtml);
            }
        }	
    });
    //-- Script para cargar la Huella dactilar del pulgar Derecho...
    new AjaxUpload('#dvHuellaDer', {
        action: url_upload,
        dataType: 'json',
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvHuellaDer").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){            
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnHuellaDer").val(result.file);
                $("#dvHuellaDer").empty();
                $("#dvHuellaDer").append(imgHtml);
            }
        }	
    });
    
    // Acci�n del bot�n Guardar...
    $('#btnGuardar').click(function(){
        if ( $('#hdnFotoIzq').val() != '' || $('#hdnFotoFte').val() != '' || $('#hdnFotoDer').val() != '' || $('#hdnFirma').val() != '' || $('#hdnHuellaIzq').val() != '' || $('#hdnHuellaDer').val() != '' ) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los documentos de identidad seleccionados?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $(this).dialog('close');
                        guardarDoctos();
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        } else {
            shwError('No ha realizado ning�n cambio', 350);
        }
    });
});

function guardarDoctos(){
    var dlgMsjWait;
    $.ajax({
        url: xDcrypt($('#hdnUrlSave').val()),
        data: $('#frmRegistro').serialize(),
        type: 'post',
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function(){
            var htmlMsj = '<div id="dvMsjWait" title="SISP :: Procesando...">';
                htmlMsj += '   <div class="dvLoading" style="height: 100px;"></div>'
                htmlMsj += '</div>';
            dlgMsjWait = $(htmlMsj);
            dlgMsjWait.dialog({
               autoOpen: true,
               draggable: true,
               modal: true,
               width: 250           
            });
            
        },
        success: function(xdata){
            dlgMsjWait.dialog('close');
            if( xdata.rslt ){
                var dlgMsj = $( getHTMLMensaje('Felicidades!!! los documentos se guardaron correctamente', 1) );
                dlgMsj.dialog({
                    autoOpen: true,
                    draggable: true,
                    modal: true,
                    width: 400,
                    buttons:{
                        'Aceptar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }
            else
                shwError(xdata.error, 450);
        },
        error: function(objeto, detalle, otroobj){                
            shwError('Lo sentimos ha ocurrido un error: ' + detalle, 450);
        }
    });
}