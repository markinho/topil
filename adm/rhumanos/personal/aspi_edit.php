<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_aspirantes.class.php';
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admcat_estado_civil.class.php';
include 'includes/class/admcat_nivel_estudios.class.php';
include 'includes/class/admcat_areas.class.php';
include 'includes/class/admcat_tipo_funciones.class.php';
include 'includes/class/admcat_especialidades.class.php';
include 'includes/class/admcat_categorias.class.php';

$objAspi = new AdmtblAspirantes();
$objMunicipio = new AdmcatMunicipios();
$objEdoCivil = new AdmcatEstadoCivil();
$objNivelEstudios = new AdmcatNivelEstudios();
$objArea = new AdmcatAreas();
$objTipoFuncion = new AdmcatTipoFunciones();
$objEspecialidad = new AdmcatEspecialidades();
$objCategoria  = new AdmcatCategorias();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/aspirante.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Reclutamiento');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('ctrl_aspi');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('aspi_rg');
    
    $_SESSION['xCurp'] = ( isset($_GET['id']) ) ? $objSys->decrypt($_GET['id']) : $_SESSION['xCurp'];
    $objAspi->select($_SESSION['xCurp']);
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">                    
                    <?php
                    if( $objAspi->status == 1 ){
                    ?>
                        <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo recluta...">
                            <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                        </a>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo recluta...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Registro</span>
            <fieldset class="fsetForm-Data" style="width: 90%;">  
                <legend>Datos Personales</legend>                              
                <table id="tbForm-DatPer" class="tbForm-Data"> 
                    <tr>
                        <td><label for="txtCurp">C.U.R.P.:</label></td>
                        <td class="validation" style="width: 400px;">
                            <input type="text" name="txtCurp" id="txtCurp" value="<?php echo $objAspi->curp;?>" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNombre">Nombre:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $objAspi->nombre;?>" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAPaterno">Apellido Paterno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAPaterno" id="txtAPaterno" value="<?php echo $objAspi->a_paterno;?>" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAMaterno">Apellido Materno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAMaterno" id="txtAMaterno" value="<?php echo $objAspi->a_materno;?>" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaNac">Fecha de Nacimiento:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaNac" id="txtFechaNac" value="<?php echo date('d/m/Y', strtotime($objAspi->fecha_nac));?>" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Sexo:</label></td>
                        <td class="validation">
                            <?php
                            if ($objAspi->genero == 1) {
                                $rbnSexo1 = 'checked="true"';
                                $rbnSexo2 = '';
                            } else {
                                $rbnSexo1 = '';
                                $rbnSexo2 = 'checked="true"';
                            } 
                            ?>
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnSexo" id="rbnSexo1" value="1" <?php echo $rbnSexo1;?> />Masculino</label>
                            <label class="label-Radio"><input type="radio" name="rbnSexo" id="rbnSexo2" value="2" <?php echo $rbnSexo2;?> />Femenino</label>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEdoCivil">Estado Civil:</label></td>
                        <td class="validation">
                            <select name="cbxEdoCivil" id="cbxEdoCivil" title="">
                                <?php
                                echo $objEdoCivil->shwEstadoCivil($objAspi->id_estado_civil);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
               
            <fieldset class="fsetForm-Data" style="width: 90%;">
                <legend>Domicilio</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="txtCalle">Nombre de la Calle:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCalle" id="txtCalle" value="<?php echo $objAspi->calle;?>" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNumExt">No. Exterior:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumExt" id="txtNumExt" value="<?php echo $objAspi->num_ext;?>" maxlength="5" title="..." style="width: 70px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                    
                    <tr>
                        <td><label for="txtColonia">Colonia:</label></td>
                        <td class="validation">
                            <input type="text" name="txtColonia" id="txtColonia" value="<?php echo $objAspi->colonia;?>" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCiudad">Ciudad o Localidad:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCiudad" id="txtCiudad" value="<?php echo $objAspi->ciudad;?>" maxlength="60" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxMpioDomicilio">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxMpioDomicilio" id="cbxMpioDomicilio" style="max-width: 500px;">
                                <?php
                                $objAspi->AdmcatMunicipios->select($objAspi->id_municipio_domi);
                                $id_entidad = $objAspi->AdmcatMunicipios->id_entidad;
                                echo $objMunicipio->shwMunicipios($objAspi->id_municipio_domi, $id_entidad); // Entidad: 12=Guerrero
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEntDomicilio">Entidad Federativa:</label></td>
                        <td class="validation">
                            <select name="cbxEntDomicilio" id="cbxEntDomicilio">                                        
                                <?php
                                echo $objMunicipio->AdmcatEntidades->shwEntidades($id_entidad, 1);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelFijo">Tel�fono Fijo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelFijo" id="txtTelFijo" value="<?php echo $objAspi->tel_fijo;?>" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtTelMovil">Tel�fono M�vil:</label></td>
                        <td class="validation">
                            <input type="text" name="txtTelMovil" id="txtTelMovil" value="<?php echo $objAspi->tel_movil;?>" maxlength="15" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>
                
            <fieldset class="fsetForm-Data" style="width: 90%;">
                <legend>Nivel de Estudios</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="cbxNivelEstudios">Nivel de Estudios:</label></td>
                        <td class="validation">
                            <select name="cbxNivelEstudios" id="cbxNivelEstudios">
                                <?php
                                echo $objNivelEstudios->shwNivelEstudios($objAspi->id_nivel_estudios);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <!--
                    <tr style="height: 35px;">
                        <td><label>Eficiencia Terminal:</label></td>
                        <td class="validation">
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="1" />Concluido</label>
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="2" />Cursando</label>
                            <label class="label-Radio"><input type="radio" name="rbnEfiTerminal" value="3" />Truncado</label>
                        </td>
                    </tr>
                    -->
                </table>
            </fieldset>
                
            <fieldset class="fsetForm-Data" style="width: 90%;">
                <legend>Datos de Adscripci�n</legend>
                <table class="tbForm-Data">
                    <tr>
                        <td class="validation" colspan="2" style="border-bottom: 1px dotted gray; padding-bottom: 12px;">
                            <label for="cbxArea">�rea de Adscripci�n:</label><br />
                            <select name="cbxArea" id="cbxArea" style="max-width: 600px;">
                                <?php
                                echo $objArea->shwAreas(70, $objAspi->id_area);
                                ?>                                        
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                            
                    <tr>
                        <td><label for="cbxTipoFuncion">Tipo de Funciones:</label></td>
                        <td class="validation">
                            <select name="cbxTipoFuncion" id="cbxTipoFuncion">
                                <?php
                                echo $objTipoFuncion->shwTipoFunciones($objAspi->id_tipo_funcion);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEspecialidad">Especialidad:</label></td>
                        <td class="validation">
                            <select name="cbxEspecialidad" id="cbxEspecialidad" style="max-width: 500px;">
                                <?php
                                echo $objEspecialidad->shwEspecialidades($objAspi->id_especialidad);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxCategoria">Categor�a Propuesta:</label></td>
                        <td class="validation">
                            <select name="cbxCategoria" id="cbxCategoria" style="max-width: 500px;">
                                <?php
                                echo $objCategoria->shwCategorias($objAspi->id_categoria);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCargo">Puesto o Cargo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCargo" id="txtCargo" value="<?php echo $objAspi->cargo;?>" maxlength="100" title="..." style="width: 450px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td><label for="cbxRegion">Regi�n:</label></td>
                        <td class="validation">
                            <select name="cbxRegion" id="cbxRegion" style="max-width: 500px;">
                                <?php
                                echo $objMunicipio->AdmcatRegiones->shwRegiones($objAspi->id_region_adscrip);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="2" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>