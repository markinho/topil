<?php
/**
 * Complemento del llamado ajax para listar los municipios dentro de un combobox. 
 * @param string id(curp), recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/admcat_municipios.class.php';
    $_SESSION["xCurpFicha"] = $_POST["id"];
    
    $archivo_pdf = 'adm/rhumanos/personal/ficha_pdf.php';
    $html = '<object id="objPdf" type="application/pdf" data="' . $archivo_pdf . '" class="dvTool-Bar" style="height: 600px; width: 100%;">
                alt: <a href="' . $archivo_pdf . '">Ficha.pdf</a>
            </object>';
    
    echo $html;
} else {
    echo "Error de Sesi�n...";
}
?>