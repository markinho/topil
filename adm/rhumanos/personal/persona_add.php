<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admcat_estado_civil.class.php';
include 'includes/class/admcat_tipo_sangre.class.php';
include 'includes/class/admcat_nivel_estudios.class.php';
include 'includes/class/admcat_areas.class.php';
include 'includes/class/admcat_tipo_funciones.class.php';
include 'includes/class/admcat_especialidades.class.php';
include 'includes/class/admcat_categorias.class.php';
include 'includes/class/admcat_nivel_mando.class.php';
include 'includes/class/admcat_horarios.class.php';
include 'includes/class/admcat_ubicaciones.class.php';

$objMunicipio = new AdmcatMunicipios();
$objEdoCivil = new AdmcatEstadoCivil();
$objTipoSangre = new AdmcatTipoSangre();
$objNivelEstudios = new AdmcatNivelEstudios();
$objArea = new AdmcatAreas();
$objTipoFuncion = new AdmcatTipoFunciones();
$objEspecialidad = new AdmcatEspecialidades();
$objCategoria  = new AdmcatCategorias();
$objNivelMando = new AdmcatNivelMando();
$objHorario = new AdmcatHorarios();
$objUbicacion = new AdmcatUbicaciones();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/persona.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display: none; width: 90px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back241.png" alt="" style="border: none;" /><br />Anterior
                    </a>                
                    <a href="#" id="btnSiguiente" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/next24.png" alt="" style="border: none;" /><br />Siguiente
                    </a>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="display: none; width: 110px;" title="Guardar los datos del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Registro</span>                                
            <div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
                <ul>
                    <li><a href="#tab-1" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Datos Personales</a></li>
                    <li><a href="#tab-2" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Domicilio</a></li>
                    <li><a href="#tab-3" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">3</span>Nivel de Estudios</a></li>
                    <li><a href="#tab-4" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">4</span>Adscripci�n</a></li>
                </ul>         
                <!-- Datos Personales -->
                <div id="tab-1">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtCurp">C.U.R.P.:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtCurp" id="txtCurp" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtNombre">Nombre:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNombre" id="txtNombre" value="" maxlength="35" title="..." style="width: 200px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtAPaterno">Apellido Paterno:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtAPaterno" id="txtAPaterno" value="" maxlength="35" title="..." style="width: 200px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtAMaterno">Apellido Materno:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtAMaterno" id="txtAMaterno" value="" maxlength="35" title="..." style="width: 200px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFechaNac">Fecha de Nacimiento:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaNac" id="txtFechaNac" value="" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Sexo:</label></td>
                                <td class="validation">
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnSexo" id="rbnSexo1" value="1" />Masculino</label>
                                    <label class="label-Radio"><input type="radio" name="rbnSexo" id="rbnSexo2" value="2" />Femenino</label>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtRfc">R.F.C.:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtRfc" id="txtRfc" value="" maxlength="14" title="..." style="width: 140px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCuip">C.U.I.P.:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCuip" id="txtCuip" value="" maxlength="20" title="..." style="width: 200px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFolioIfe">Folio IFE:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFolioIfe" id="txtFolioIfe" value="" maxlength="20" title="..." style="width: 150px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCartilla">Matr�cula del S.M.N.:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCartilla" id="txtCartilla" value="" maxlength="10" title="..." style="width: 100px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtLicencia">Licencia de Conducir:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtLicencia" id="txtLicencia" value="" maxlength="11" title="..." style="width: 100px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtPasaporte">Pasaporte:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtPasaporte" id="txtPasaporte" value="" maxlength="10" title="..." style="width: 100px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxEdoCivil">Estado Civil:</label></td>
                                <td class="validation">
                                    <select name="cbxEdoCivil" id="cbxEdoCivil" title="">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objEdoCivil->shwEstadoCivil();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxTipoSangre">Grupo Sanguineo:</label></td>
                                <td class="validation">
                                    <select name="cbxTipoSangre" id="cbxTipoSangre">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objTipoSangre->shwTipoSangre();
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left;">
                                    <fieldset class="fsetForm-Data" style="border: 1px solid #c1c1bf; width: 590px;">
                                        <legend style="border: 1px solid #c1c1bf; font-size: 9pt;">Lugar de Nacimiento</legend>        
                                        <table class="tbForm-Data">
                                            <tr>
                                                <td><label for="cbxEntNacimiento">Entidad Federativa:</label></td>
                                                <td class="validation">
                                                    <select name="cbxEntNacimiento" id="cbxEntNacimiento">
                                                        <option value="0">&nbsp;</option>
                                                        <?php
                                                        echo $objMunicipio->AdmcatEntidades->shwEntidades(0, 1);// Pa�s: 1=M�xico
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label for="cbxMpioNacimiento">Municipio:</label></td>
                                                <td class="validation">
                                                    <select name="cbxMpioNacimiento" id="cbxMpioNacimiento" style="max-width: 500px;">
                                                        <option value="0"></option>
                                                    </select>                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label for="txtLugarNacimiento">Ciudad o Localidad:</label></td>
                                                <td class="validation">
                                                    <input type="text" name="txtLugarNacimiento" id="txtLugarNacimiento" value="" maxlength="200" title="..." style="width: 350px;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                
                <!-- Domicilio -->
                <div id="tab-2">
                    <fieldset class="fsetForm-Data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="txtCalle">Nombre de la Calle:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCalle" id="txtCalle" value="" maxlength="60" title="..." style="width: 350px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtNumExt">No. Exterior:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNumExt" id="txtNumExt" value="" maxlength="5" title="..." style="width: 70px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtNumInt">No. Interior:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNumInt" id="txtNumInt" value="" maxlength="5" title="..." style="width: 70px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtEntreCalle1">Entre la calle:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtEntreCalle1" id="txtEntreCalle1" value="" maxlength="60" title="..." style="width: 350px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtEntreCalle2">Y la calle:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtEntreCalle2" id="txtEntreCalle2" value="" maxlength="60" title="..." style="width: 350px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtColonia">Colonia:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtColonia" id="txtColonia" value="" maxlength="60" title="..." style="width: 350px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCiudad">Ciudad o Localidad:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCiudad" id="txtCiudad" value="" maxlength="60" title="..." style="width: 350px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCodPostal">C�digo Postal:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCodPostal" id="txtCodPostal" value="" maxlength="5" title="..." style="width: 70px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxEntDomicilio">Entidad Federativa:</label></td>
                                <td class="validation">
                                    <select name="cbxEntDomicilio" id="cbxEntDomicilio">                                        
                                        <?php
                                        echo $objMunicipio->AdmcatEntidades->shwEntidades(12, 1);
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxMpioDomicilio">Municipio:</label></td>
                                <td class="validation">
                                    <select name="cbxMpioDomicilio" id="cbxMpioDomicilio" style="max-width: 500px;">    
                                        <option value="0"></option>                                    
                                        <?php
                                        echo $objMunicipio->shwMunicipios(0, 12); // Entidad: 12=Guerrero
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtTelFijo">Tel�fono Fijo:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtTelFijo" id="txtTelFijo" value="" maxlength="15" title="..." style="width: 150px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtTelMovil">Tel�fono M�vil:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtTelMovil" id="txtTelMovil" value="" maxlength="15" title="..." style="width: 150px;" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                
                <!-- Nivel de Estudios -->
                <div id="tab-3">
                    <fieldset class="fsetForm-Data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="cbxNivelEstudios">Nivel de Estudios:</label></td>
                                <td class="validation">
                                    <select name="cbxNivelEstudios" id="cbxNivelEstudios">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objNivelEstudios->shwNivelEstudios();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>                            
                            <tr style="height: 35px;">
                                <td><label>Eficiencia Terminal:</label></td>
                                <td class="validation">
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="1" />Concluido</label>
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="2" />Cursando</label>
                                    <label class="label-Radio"><input type="radio" name="rbnEfiTerminal" value="3" />Truncado</label>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtInstitucion">Instituci�n:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtInstitucion" id="txtInstitucion" value="" maxlength="100" title="..." style="width: 400px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCct">C.C.T.:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCct" id="txtCct" value="" maxlength="15" title="..." style="width: 170px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCarrera">Carrera:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCarrera" id="txtCarrera" value="" maxlength="100" title="..." style="width: 400px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtEspecialidad">Especialidad:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtEspecialidad" id="txtEspecialidad" value="" maxlength="100" title="..." style="width: 400px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtDocumento">Documento que avala:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtDocumento" id="txtDocumento" value="" maxlength="45" title="..." style="width: 250px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFolioDocto">Folio del documento:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFolioDocto" id="txtFolioDocto" value="" maxlength="10" title="..." style="width: 80px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCedula">No. de C�dula:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCedula" id="txtCedula" value="" maxlength="7" title="..." style="width: 80px;" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                
                <!-- Adscripci�n -->
                <div id="tab-4">
                    <fieldset class="fsetForm-Data">
                        <table class="tbForm-Data">
                            <tr>
                                <td class="validation" colspan="2" style="border-bottom: 1px dotted gray; padding-bottom: 12px;">
                                    <label for="cbxArea">�rea de Adscripci�n:</label><br />
                                    <select name="cbxArea" id="cbxArea" style="max-width: 600px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objArea->shwAreas(70);
                                        ?>                                        
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>                            
                            <tr>
                                <td><label for="cbxTipoFuncion">Tipo de Funciones:</label></td>
                                <td class="validation">
                                    <select name="cbxTipoFuncion" id="cbxTipoFuncion">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objTipoFuncion->shwTipoFunciones();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxEspecialidad">Especialidad:</label></td>
                                <td class="validation">
                                    <select name="cbxEspecialidad" id="cbxEspecialidad" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objEspecialidad->shwEspecialidades();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxCategoria">Categor�a Asignada:</label></td>
                                <td class="validation">
                                    <select name="cbxCategoria" id="cbxCategoria" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objCategoria->shwCategorias();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCargo">Puesto o Cargo:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtCargo" id="txtCargo" value="" maxlength="100" title="..." style="width: 450px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxNivelMando">Nivel de Mando:</label></td>
                                <td class="validation">
                                    <select name="cbxNivelMando" id="cbxNivelMando" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objNivelMando->shwNivelMando();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFechaIng">Fecha de Ingreso:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaIng" id="txtFechaIng" value="" maxlength="35" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxHorario">Horario Laboral:</label></td>
                                <td class="validation">
                                    <select name="cbxHorario" id="cbxHorario" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objHorario->shwHorarios();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxRegion">Regi�n:</label></td>
                                <td class="validation">
                                    <select name="cbxRegion" id="cbxRegion" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objMunicipio->AdmcatRegiones->shwRegiones();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxMpioAdscripcion">Municipio:</label></td>
                                <td class="validation">
                                    <select name="cbxMpioAdscripcion" id="cbxMpioAdscripcion" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxUbicacion">Ubicaci�n:</label></td>
                                <td class="validation">
                                    <select name="cbxUbicacion" id="cbxUbicacion" style="max-width: 500px;">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objUbicacion->shwUbicaciones();
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="1" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
        <input type="hidden" name="dtCurpAspi" id="hdnCurpAspi" value="" />
    </form>
    
    <div id="dvLista-Aspi" title="SISP :: Selecci�n de reclutas">
        <div id="dvGrid-Aspi" style="border: none; height: 300px; margin: auto auto; margin-top: 10px; width: 960px;">
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbSearch">
                    <tr>
                        <td>Buscar: <input type="text" name="txtBuscar" size="25" value="" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                    </tr>
                </table>
                <table class="xGrid-tbCols">
                    <tr>  
                        <th style="width: 5%; text-align: center;">&nbsp;</th>
                        <th style="width: 30%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                        <th style="width: 18%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                        <th style="width: 22%;" class="xGrid-tbCols-ColSortable">�REA</th>                        
                        <th style="width: 10%;" class="xGrid-thNo-Class">&nbsp;</th>
                    </tr>
                </table>
            </div>
            <div class="xGrid-dvBody">
            
            </div>        
        </div>
        <input type="hidden" id="hdnUrlDatAspiGrid" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_datos_aspi_grid_min.php');?>" />
        <input type="hidden" id="hdnUrlDatAspi" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_datos_aspi.php');?>" />
    </div>    
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>